from django.contrib import admin

from account.models import NewsLetter, SentMailLog


class NewsLetterAdmin(admin.ModelAdmin):
    list_display = ['email', 'created_time']


class SentMailLogAdmin(admin.ModelAdmin):
    search_fields = ('user__username',)
    list_display = ['user', 'mail_type', 'sent_time']
    list_filter = ['mail_type']


admin.site.register(NewsLetter, NewsLetterAdmin)
admin.site.register(SentMailLog, SentMailLogAdmin)
