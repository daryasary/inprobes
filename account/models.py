from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from account.tasks import send_confirm_email_task


class NewsLetter(models.Model):
    email = models.EmailField()
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('NewsLetter')
        verbose_name_plural = _('NewsLetters')

    def __unicode__(self):
        return self.email


class SentMailLog(models.Model):
    CONFIRMATION = 1
    MAILS_TYPES = (
        (CONFIRMATION, _("Confirmation")),
    )
    user = models.ForeignKey(User, related_name='sent_mail')
    mail_type = models.IntegerField(choices=MAILS_TYPES, default=CONFIRMATION)
    sent_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('SentMailLog')
        verbose_name_plural = _('SentMailLogs')

    def __unicode__(self):
        return "{}: {}".format(self.user.username, self.sent_time)

    @classmethod
    def send(cls, user, host, mail_type=1):
        last_mail = cls.objects.filter(user=user, mail_type=mail_type).last()
        if last_mail is not None and (timezone.now() - last_mail.sent_time).seconds < 120:
            return

        cls.objects.create(user=user, mail_type=mail_type)
        send_confirm_email_task.apply_async((user.id, host), queue='high')
