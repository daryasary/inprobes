from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^password_reset/$', views.password_reset, name='password_reset'),
    url(r'^reset_password_confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.reset_password_confirm,
        name='password_reset_confirm'),
    url(r'^complete_registration/$', views.complete_registration, name='complete_registration'),
    url(r'send_mass_mail', views.send_to_all, name='send_mail'),
    url(r'confirm_email/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.confirm_email, name='confirm_email'),
    url(r'^newsletter/subscribe/$', views.subscribe_newsletter, name='subscribe_newsletter'),
]
