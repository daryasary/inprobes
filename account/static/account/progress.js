function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function getData(){
        $.ajax( {
                url: "/dashboard/api/get_progress/",
                type: 'GET',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                },
                success: function( data ) {
                  $("#accountProgress").css('width',data.percentage+'%');
                  $("#accountProgress").html(data.percentage+'%');
                  $("#progressData").html(data.current_count+'/'+data.media_count);

                  if(data.percentage > 99.999) {
                    $("#dashboardRoute").removeClass("invisible");
                    $("#progress").html("Done");
                  }
                }
            })
};
getData();
$(document).ready(function(){
    var progresspump = setInterval(function(){
        $.ajax( {
                url: "/dashboard/api/get_progress/",
                type: 'GET',
                beforeSend : function( xhr ) {
                    xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
                },
                success: function( data ) {
                  $("#accountProgress").css('width',data.percentage+'%');
                  $("#accountProgress").html(data.percentage+'%');
                  $("#progressData").html(data.current_count+'/'+data.media_count);

                  if(data.percentage > 99.999) {
                    clearInterval(progresspump);
                    $("#dashboardRoute").removeClass("invisible");
                    $("#progress").html("Done");
                  }
                }
            })
    }, 2000)
    }
);