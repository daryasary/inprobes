from django.contrib import messages
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_decode
from django.template import loader
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.views.decorators.http import require_POST

from account.forms import MassMailForm
from account.models import NewsLetter, SentMailLog
from account.tasks import send_confirm_email_task, send_password_reset_link
from financial.models import AccountPackage

from home.settings import DEFAULT_FROM_EMAIL

from index.models import UserProfile


def password_reset(request):
    context = {
        'messages': [],
        'title': 'Password Reset'
    }
    if request.method == 'POST':
        user_input = request.POST.get('email')
        try:
            validate_email(user_input)
            email = user_input
        except ValidationError:
            username = user_input
            email = False

        if email:
            # User entered an email in the field though:
            users = User.objects.filter(Q(email=email) | Q(username=email))
        else:
            # User entered a username
            users = User.objects.filter(Q(username=username))
        if users.exists():
            for user in users:
                send_password_reset_link.apply_async((user.id, request.META['HTTP_HOST']), queue='high')
                context['messages'].append(_('A recovery  message sent to %(email)s address') % {'email': user.email})
        else:
            context['messages'].append(_('No user associated with %(email)s email or username') % {'email': user_input})

    else:
        context['messages'].append(_('Enter your username or email below please'))

    return render(request, 'account/password_reset.html', {'context': context})


def reset_password_confirm(request, uidb64=None, token=None):
    context = {
        'messages': [],
        'title': 'Password Reset'
    }
    assert uidb64 is not None and token is not None
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        if request.method == 'POST':
            p1 = request.POST.get('password1')
            p2 = request.POST.get('password2')
            if p1 == p2:
                user.set_password(p1)
                user.save()
                context['messages'].append(_('Your password changed successfully'))
            else:
                context['messages'].append(_('Passwords are not the same!'))
        else:
            context['messages'].append(_('Enter new password'))
    else:
        context['errors'] = _('Sorry, the reset password link is no longer valid.')

    return render(request, 'account/password_reset_confirm.html', {'context': context})


@login_required
@staff_member_required
def send_to_all(request):
    if request.method == 'POST':
        form = MassMailForm(request.POST)
        if form.is_valid():
            users = User.objects.all()
            context = {'subject': form.cleaned_data['subject'], 'message': form.cleaned_data['body']}
            subject_template = 'account/email_subject.txt'
            message_template = 'account/email_template.html'
            for user in users:
                context['subject'] = loader.render_to_string(context['subject'], context)
                context['username'] = user.username
                context['header'] = user.username
                subject = loader.render_to_string(subject_template, context)
                subject = ''.join(subject.splitlines())
                body = loader.render_to_string(message_template, context)
                send_mail(subject, body, DEFAULT_FROM_EMAIL, [user.email])
    form = MassMailForm()
    return render(request, 'account/send_to_all.html', {'form': form})


@login_required
def complete_registration(request):
    """Complete or Modify user registration steps"""
    user = request.user
    profile = user.userprofile
    if request.GET.get('parsed', False):
        profile.parsed = True
        profile.save()
    if not profile.confirm:
        SentMailLog.send(user, request.META['HTTP_HOST'])
        messages.info(request,
                      _('A confirmation  message sent to your email please '
                        'check your inbox and follow the instructions'))
    elif profile.parsed and profile.has_facebook and profile.primary_account:
        return redirect(reverse('dashboard'))

    context = {'profile': profile}
    return render(request, 'account/complete_registration_v2.html', context)


def confirm_email(request, uidb64=None, token=None):
    """Confirm user email with given data"""
    context = {
        'message': [],
        'title': 'Confirm Email'
    }
    assert uidb64 is not None and token is not None
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        confirm = UserProfile.confirm_user(user.id)
        if confirm:
            messages.info(request, _('Your email confirmed successfully'))
        else:
            messages.info(request, _(
                'Sorry, Your email not confirmed please try again'
            ))
    else:
        messages.info(request, _('Sorry, Link is not valid!'))

    return render(request, 'account/email_confirm.html', {'context': context})


def subscribe_newsletter(request):
    email = request.GET.get('email', None)
    response = _('Enter email to subscribe newsletter')
    if email:
        try:
            validate_email(email)
        except ValidationError:
            response = _('Enter a valid email adderss')
        else:
            subs, result = NewsLetter.objects.get_or_create(email=email)
            if result:
                response = _('Added successfully')
            else:
                response = _('Already exists')
    return JsonResponse({'message': str(response)})
