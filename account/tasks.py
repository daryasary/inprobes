from __future__ import absolute_import

from django.contrib.auth.models import User
from django.contrib.auth.tokens import default_token_generator
from django.core.mail import send_mail
from django.template import loader
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from celery.task.schedules import crontab
from celery.decorators import task, periodic_task
from celery.utils.log import get_task_logger

from account.utils import update
from index.models import UserProfile

from services.InstagramData.utils import AccountHandler

logger = get_task_logger(__name__)
DEFAULT_FROM_EMAIL = settings.DEFAULT_FROM_EMAIL


@task(name="Send confirm email task")
def send_confirm_email_task(user_id, host):
    """sends an email when user give access to his instagram
    account, to confirm email address"""
    logger.info("Sent confirmation email")
    user = User.objects.get(id=user_id)

    if user and not user.userprofile.confirm:
        dic = {'email': user.email, 'domain': host, 'site_name': _('InProbes'),
               'uid': urlsafe_base64_encode(force_bytes(user.pk)), 'user': user,
               'token': default_token_generator.make_token(user),
               'protocol': 'http', 'username': user.get_username()}
        subject = _('InProbes:Confirm your email address')
        email_template_name = 'account/confirm_email_template.html'
        email = loader.render_to_string(email_template_name, dic)
        html_message = loader.render_to_string(
            'account/confirm_email_template_html.html', dic)
        send_mail(subject, email, DEFAULT_FROM_EMAIL, [user.email],
                  html_message=html_message)
        return True
    return False


@task(bind=True, name='Send password reset link')
def send_password_reset_link(self, user_id, host):
    """send an email contain a password reset link"""
    # logger.info("Sent confirmation email")
    user = User.objects.get(id=user_id)
    if user:
        dic = {
            'email': user.email,
            'domain': host,
            'site_name': _('InProbes'),
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'username': user.get_username(),
            'token': default_token_generator.make_token(user),
            'protocol': 'http',
        }
        subject_template_name = 'account/password_reset_subject.txt'
        email_template_name = 'account/password_reset_email.html'
        subject = loader.render_to_string(subject_template_name, dic)
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, dic)
        template = loader.render_to_string(
            'account/password_reset_email_template.html', dic)
        send_mail(subject, email, 'support@inprobes.com', [user.email],
                  html_message=template)
        return True, user.email
    return False, 'django user #{}'.format(user_id)


# TODO: Task disabled, may remove later
# @task(name="Update user account")
def update_user_account_task(user_id):
    """Update single user account"""
    # logger.info("Store data for site user number %s" % user_id)
    return update(user_id)


# TODO: Task disabled, may remove later
# @periodic_task(
#     run_every=(crontab(minute=40, hour='0,12,18')),
#     name="account_manager", ignore_result=True
# )
def account_manager():
    """Update All users Accounts periodically"""
    profiles_id = UserProfile.objects.values_list(
        'id', flat=True).exclude(instagram_id__isnull=True)
    for pid in profiles_id:
        account_handler.delay(pid)

    # logger.info('Account manager created {} task.'.format(len(profiles_id)))
    return len(profiles_id)


# TODO: Task disabled, may remove later
# @task(name='Account handler')
def account_handler(account_id):
    try:
        profile = UserProfile.objects.get(pk=account_id)
    except UserProfile.DoesNotExist:
        result = False
        profile = None
    else:
        # logger.info("Store data for profile: %s" % profile.user)
        level = profile.level
        if level == 'L':
            result = AccountHandler.update_basic_account(profile.instagram_id)
        elif level == 'M':
            result = AccountHandler.update_advanced_account(
                profile.instagram_id
            )
        elif level == 'H':
            result = AccountHandler.update_professional_account(
                profile.instagram_id
            )
        else:
            result = False

        # TODO: Request new access token for user
        if result == "Instagram API Error":
            profile.set_instageram_id_to_null()
            logger.warning(
                "Profile %s encountered Instagram API Error" % profile.user
            )
    return 'profile {} : {}'.format(profile, result)
