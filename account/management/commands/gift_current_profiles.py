from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.shortcuts import get_object_or_404
from django.utils import timezone

from financial.models import AccountPackage, AccountPurchase
from payment.models import Payment


class Command(BaseCommand):

    def handle(self, *args, **options):
        for user in User.objects.filter(userprofile__isnull=False):
            print("User: {}, Profile: {}".format(user.username, user.userprofile))
            package = get_object_or_404(AccountPackage, pk=4)
            payment_data = {'user': user, 'amount': package.price,
                            'email': user.email, 'is_paid': True}
            payment = Payment.objects.create(**payment_data)
            purchase_data = {
                'package': package, 'payment': payment, 'user': user,
                'valid': True, 'expire_time': timezone.now() + package.delta
            }
            purchase = AccountPurchase.objects.create(**purchase_data)
            user.userprofile.update_level(purchase.package.level)
