
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.core.mail import send_mail
from django.template import loader

from index.models import UserProfile


class Command(BaseCommand):

    def handle(self, *args, **options):
        subject = '\xd8\xb1\xd8\xa7\xd9\x87 \xd8\xa7\xd9\x86\xd8\xaf\xd8\xa7\xd8\xb2\xdb\x8c \xd9\x86\xd8\xb3\xd8\xae\xd9\x87 \xd8\xad\xd8\xb1\xd9\x81\xd9\x87\xe2\x80\x8c\xd8\xa7\xdb\x8c \xd9\xbe\xd9\x86\xd9\x84 \xd8\xa7\xdb\x8c\xd9\x86\xd9\xbe\xd8\xb1\xd9\x88\xd8\xa8\xd8\xb2 + \xd8\xa8\xd8\xb3\xd8\xaa\xd9\x87 \xdb\x8c\xda\xa9 \xd9\x85\xd8\xa7\xd9\x87\xd9\x87 \xd9\x87\xd8\xaf\xdb\x8c\xd9\x87 \xd8\xb4\xd9\x85\xd8\xa7'
        profiles = UserProfile.objects.all().exclude(user__username__in=['farmad', 'iefazati', 'm2sh', 'Halili'])
        for profile in profiles:
            dic = {'username': profile.user.username}
            # email = loader.render_to_string(email_template_name, dic)
            email = 'dear {{username}}'
            html_message = loader.render_to_string(
                'account/main_temp.html', dic)
            send_mail(
                subject, email, 'support@inprobes.com',
                [profile.user.email], html_message=html_message
            )
            print 'sent email to %s' %profile.user.email



