# coding=utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _

class MassMailForm(forms.Form):
    subject = forms.CharField(label=_('Subject'), widget=forms.TextInput(attrs={'class': 'form-control'}))
    body = forms.CharField(label=_('Subject'), widget=forms.Textarea(attrs={'class': 'form-control area-place'}))