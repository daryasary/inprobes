from django.contrib.auth.models import User

from index.models import UserProfile
from services.InstagramData.utils import AccountHandler
from statistics.utils.compute import RedisCache


def update(user_id):
    """Tool to be called form a task, and update account"""
    profile = UserProfile.load(user_id=user_id)
    level = profile.level
    if level == 'L':
        result = AccountHandler.update_basic_account(profile.instagram_id)
    elif level == 'M':
        result = AccountHandler.update_advanced_account(profile.instagram_id)
    elif level == 'H':
        result = AccountHandler.update_professional_account(profile.instagram_id)
    else:
        result = False

    if result == "Instagram API Error":
        profile.set_instageram_id_to_null()
        return result
    elif result:
        # user = User.objects.get(id=user_id)
        UserProfile.set_parsed(user_id)
        # RedisCache.cache(user)
    return result
