# Usage
    # In MySQL:
    CREATE DATABASE inprobes_db
        CHARACTER SET utf8mb4
        COLLATE utf8mb4_unicode_ci;
        
    # In shell:
    python manage.py migrate

## Create a new file : home/private_settings.py:

        # coding=utf-8
        """Applications Private Setting that would't be stored on git server"""

        # Instagram API
        API_CONFIG = {
            'client_id': '',
            'client_secret': '',
            'redirect_uri': '',
        }

        FULL_PERMISSIONS = ['basic', 'public_content', 'follower_list', 'likes', 'relationships', 'comments']

        CUSTOM_DATABASE = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                # 'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
                'NAME': 'name',
                'HOST': 'localhost',
                'USER': 'username',
                'PASSWORD': 'password',
                'PORT': '',
                'OPTIONS': {'charset': 'utf8mb4'},
            }
        }

        DEBUG_MODE = True
        PRIVATE_ALLOWED_HOSTS = ['localhost', '127.0.0.1', '95.211.228.204', 'inprobes.com']

## Run celery (local server)
    celery -A home worker -Q high --hostname=z@%h -l info
    celery -A home worker -Q high,low --hostname=x@%h -l info
    celery -A home beat -l info

