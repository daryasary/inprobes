# -*- coding: utf-8 -*-
# !/usr/bin/env python
"""
Minimal Example
===============
Generating a square wordcloud from the US constitution using default arguments.
"""
import re
import codecs
import sys

from os import path

from persian_wordcloud import PersianWordCloud, add_stop_words

from report.models import HashtagReport

d = path.dirname(__file__)

reload(sys)
sys.setdefaultencoding('utf-8')


def test_wc():
    # text = codecs.open(path.join(d, 'persian.txt'), encoding='utf-8').read()
    # codecs.ascii_/encode()
    # Add another stopword
    # stopwords = add_stop_words(['کاسپین'])
    stopwords = add_stop_words([])
    # add_stop_words

    report = HashtagReport.objects.get(id=2)
    media = report.tag.media.all()
    captions = media.values_list('caption__text', flat=True)
    # cp = codecs.encode(' '.join(captions), 'utf-8', 'replace')

    # Can not remove all emoji characters
    emoji_pattern = re.compile("["
                               u"\U0001F600-\U0001F64F"  # emoticons
                               u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                               u"\U0001F680-\U0001F6FF"  # transport & map symbols
                               u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               "]+", flags=re.UNICODE)
    txt = emoji_pattern.sub(r'', ' '.join(captions))

    # Generate a word cloud image
    wordcloud = PersianWordCloud(
        only_persian=True,
        max_words=100,
        stopwords=stopwords,
        margin=0,
        width=800,
        height=800,
        min_font_size=1,
        max_font_size=500,
        background_color="white"
    ).generate(txt)

    image = wordcloud.to_image()
    image.show()
    image.save('result.png')
