from django.core.urlresolvers import reverse
from django.shortcuts import redirect


def validate_account(func):
    """When user tries to access one of dashboards views, we will check wether
    his/her account is valid yet or no?!, if is not valid user will be
    redirected to the complete registration page and fix problem there"""

    def wrapper(request, *args, **kwargs):
        profile = request.user.userprofile
        if not profile.has_facebook or profile.primary_account is None \
                or not profile.parsed or not profile.confirm:
            return redirect(reverse('complete_registration'))
        return func(request, *args, **kwargs)

    wrapper.__doc__ = func.__doc__
    wrapper.__name__ = func.__name__
    return wrapper
