from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class BlogConfig(AppConfig):
    name = 'Blog'
    verbose_name = _('Blog')
    verbose_name_plural = _('Blogs')
