from django.contrib.sitemaps import ping_google
from django.db import models
from django.utils.text import slugify
from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from redactor.fields import RedactorField


class Author(models.Model):
    # Display_name should be available for translation
    user = models.OneToOneField(User, related_name='author')
    display_name = models.CharField(max_length=128)
    avatar = models.ImageField(upload_to='authors/', blank=True)

    class Meta:
        verbose_name = _("Author")
        verbose_name_plural = _("Authors")

    def __unicode__(self, *args, **kwargs):
        return self.display_name


class Category(models.Model):
    name = models.CharField(max_length=30)
    slug = models.SlugField(blank=True, unique=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def save(self, *args, **kwargs):
        # if not self.id:
        #     self.slug = slugify(self.name)
        super(Category, self).save(*args, **kwargs)

    def __unicode__(self):
        # return "%s , %s" % (self.name, self.slug)
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('blog_post_list_cat', args=[str(self.slug)])


class Tag(models.Model):
    name = models.CharField(max_length=30)
    slug = models.SlugField(blank=True)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def save(self, *args, **kwargs):
        # if not self.id:
        #     self.slug = slugify(self.name)
        super(Tag, self).save(*args, **kwargs)

    def __unicode__(self):
        # return "%s , %s" % (self.name, self.slug)
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('blog_post_list_tag', args=[str(self.slug)])


# editor added for body in post, more information
# in : github.com/douglasmiranda/django-wysiwyg-redactor
class Post(models.Model):
    header = models.ImageField(upload_to='headers/')
    title = models.CharField(max_length=255)
    slug = models.SlugField(blank=True)

    body = RedactorField()
    seo_abs = models.TextField(blank=True)

    cat = models.ManyToManyField(Category)
    tag = models.ManyToManyField(Tag)

    publish = models.BooleanField(default=True)
    comments_off = models.BooleanField(default=True)

    lang = models.CharField(max_length=2, choices=settings.LANGUAGES,
                            default='en', verbose_name=_('Language'))

    author = models.ForeignKey(Author, editable=False)
    viewed = models.PositiveIntegerField(
        verbose_name=_('Views'), default=0, editable=False
    )

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-created_at']
        verbose_name = _("Post")
        verbose_name_plural = _("Posts")

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)

        if self.publish:
            self.call_google()

    @staticmethod
    def call_google():
        try:
            ping_google()
        except Exception:
            # Bare 'except' because we could get a variety
            # of HTTP-related exceptions.
            pass

    def __unicode__(self):
        return "%s" % self.title

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('blog_single', args=[str(self.slug)])

    def get_tags(self):
        T = self.tag.all()
        return ', '.join(t.name for t in T)
    get_tags.short_description = 'Tag (s)'

    def get_cat(self):
        C = self.cat.all()
        return ', '.join(c.name for c in C)
    get_cat.short_description = 'Category'

    def incr_view(self):
        self.viewed += 1
        self.save()


class Comment(models.Model):
    name = models.CharField(max_length=30)
    email = models.EmailField()
    website = models.URLField(blank=True)
    post = models.ForeignKey(Post, related_name='comments')
    body = models.TextField()

    date = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = _("Comment")
        verbose_name_plural = _("Comments")

    def __unicode__(self):
        # return "%s, %s" % (self.name, self.body)
        return self.name
