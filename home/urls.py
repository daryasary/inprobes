"""home URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
from django.contrib.sitemaps.views import sitemap

from solid_i18n.urls import solid_i18n_patterns

from home.sitemaps import PagesSitemap, StaticSitemap, BlogSitemap


sitemaps = {
    'pages': PagesSitemap(),
    'static': StaticSitemap(),
    'blog': BlogSitemap(),
}
urlpatterns = [
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^secureadmin/', admin.site.urls),
    url(r'^redactor/', include('redactor.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if 'rosetta' in settings.INSTALLED_APPS:
    urlpatterns.append(
        url(r'^rosetta/', include('rosetta.urls')),
    )

urlpatterns += solid_i18n_patterns(
    url(r'^account/', include('account.urls')),
    url(r'^financial/', include('financial.urls')),
    url(r'^payment/', include('payment.urls')),
    url(r'^report/', include('report.urls')),
    url(r'^blog/', include('blog.urls')),
    url(r'^support/', include('tickets.urls')),
    url(r'^dashboard/', include('dashboard.urls')),
    url(r'^', include('index.urls')),
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps},
        name='django.contrib.sitemaps.views.sitemap'),
)
