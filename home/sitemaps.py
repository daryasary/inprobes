# coding=utf-8
from django.contrib.sitemaps import Sitemap

from blog.models import Post
from index.models import Page
from django.core.urlresolvers import reverse


class PagesSitemap(Sitemap):
    """Sitemap for pages"""
    changefreq = "weekly"
    priority = 0.7

    def items(self):
        return Page.objects.all()

    def lastmod(self, item):
        return item.updated


class StaticSitemap(Sitemap):
    """Sitemap for static pages"""
    priority = 0.8
    changefreq = 'weekly'

    def items(self):
        return ['home', 'register', 'user_login', 'user_logout', 'prices']

    def location(self, item):
        return reverse(item)


class BlogSitemap(Sitemap):
    changefreq = "weekly"
    priority = 0.5

    def items(self):
        return Post.objects.all()

    def lastmod(self, obj):
        return obj.created_at
