from django.contrib import admin

from campaign.models import HashtagCampaign, CommentCampaign


class HashtagCampaignAdmin(admin.ModelAdmin):
    list_display = ['user', 'account', 'tag', 'expire_date']


class CommentCampaignAdmin(admin.ModelAdmin):
    list_display = ['user', 'account', 'media', 'description']


admin.site.register(HashtagCampaign, HashtagCampaignAdmin)
admin.site.register(CommentCampaign, CommentCampaignAdmin)
