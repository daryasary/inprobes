# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2019-02-04 14:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('index', '0025_auto_20190131_2012'),
        ('graph_api', '0006_tag'),
    ]

    operations = [
        migrations.CreateModel(
            name='HashtagCampaign',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('expire_date', models.DateTimeField(blank=True, null=True, verbose_name='Expire date')),
                ('should_tag_user', models.BooleanField(default=False)),
                ('active', models.BooleanField(default=True, verbose_name='Active')),
                ('busy', models.BooleanField(default=False, verbose_name='Busy')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Modified')),
                ('account', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hashtag_campaigns', to='graph_api.Account', verbose_name='Account')),
                ('tag', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hashtag_reports', to='graph_api.Tag', verbose_name='Hashtag')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='hashtag_campaigns', to='index.UserProfile', verbose_name='User')),
            ],
            options={
                'verbose_name': 'HashtagReport',
                'verbose_name_plural': 'HashtagReports',
            },
        ),
    ]
