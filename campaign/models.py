from __future__ import unicode_literals

from django.db import models
from django.db.models import Count

from django.utils.translation import ugettext_lazy as _
from index.models import UserProfile
from services.graph_api.models import Tag, Account, Media


class HashtagCampaign(models.Model):
    user = models.ForeignKey(
        UserProfile, verbose_name=_('User'), related_name='hashtag_campaigns'
    )
    account = models.ForeignKey(
        Account, verbose_name=_("Account"), related_name='hashtag_campaigns'
    )
    tag = models.ForeignKey(
        Tag, verbose_name=_('Hashtag'), related_name='hashtag_reports'
    )
    expire_date = models.DateTimeField(
        verbose_name=_('Expire date'), null=True, blank=True
    )
    should_tag_user = models.BooleanField(default=False)
    like_rate = models.SmallIntegerField(default=0)
    comment_rate = models.SmallIntegerField(default=0)
    active = models.BooleanField(default=True, verbose_name=_('Active'))
    busy = models.BooleanField(verbose_name=_('Busy'), default=False)

    created = models.DateTimeField(verbose_name=_('Created'), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_('Modified'), auto_now=True)

    class Meta:
        verbose_name = _('HashtagCampaign')
        verbose_name_plural = _('HashtagCampaigns')

    def __unicode__(self):
        return '{} > {}'.format(self.user, self.tag.name)

    @property
    def hashtag(self):
        return "#" + self.tag.name

    @property
    def participants(self):
        return self.tag.media.values('owner').annotate(dd=Count('owner')) or []

    def media_count(self):
        return self.tag.media.count()
    media_count.short_description = _("Media count")

    def set_busy(self):
        self.busy = True
        self.save()

    def set_free(self):
        self.busy = False
        self.save()


class CommentCampaign(models.Model):
    user = models.ForeignKey(
        UserProfile, verbose_name=_('User'), related_name='comment_campaigns'
    )
    account = models.ForeignKey(
        Account, verbose_name=_("Account"), related_name='comment_campaigns'
    )
    media = models.ForeignKey(Media, related_name='campaigns')
    description = models.TextField(blank=True)

    created = models.DateTimeField(verbose_name=_('Created'), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_('Modified'), auto_now=True)

    class Meta:
        verbose_name = _('CommentCampaign')
        verbose_name_plural = _('CommentCampaigns')

    def __unicode__(self):
        return 'user: {}\tmedia:{}'.format(self.user, self.media.id)
