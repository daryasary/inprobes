from django.core.management.base import BaseCommand
from index.models import *
from utils.instagram_graph.handlers import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        profile = UserProfile.objects.first()
        account_handler = InstagramAccountsList(profile.facebook_token)
        print account_handler.graph.get()
