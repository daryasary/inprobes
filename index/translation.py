from modeltranslation.translator import translator, TranslationOptions
from .models import Page


class PageTranslationOptions(TranslationOptions):
    fields = ('header', 'body')


translator.register(Page, PageTranslationOptions)
