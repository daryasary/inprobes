from __future__ import division

import redis
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.core.validators import validate_email
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db import connection
from django.db.models import Sum, Avg, Count
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.conf import settings as app_settings
from django.views.generic import TemplateView

from utils.wc_handler import test_wc
from report.models import HashtagReport
from statistics.utils.core import CSum
from .models import Page, UserProfile
from .forms import UserForm, UserProfileForm

from services.InstagramData.utils import InstagramHandler, HashtagHandler
from services.InstagramData.models import Media, User as InstagramUser, Tag

from financial.models import AccountPackage

from constance import config

from statistics.utils.compute import RedisCache


def home(request):
    if app_settings.DEBUG:
        context = {'all_users': '10',
                   'instagram_users': '10',
                   'instagram_media': '10',
                   'title': 'home',
                   'nav_key': 'home'}
    else:
        data = redis.StrictRedis()
        context = {'all_users': data.get('all_users'),
                   'instagram_users': data.get('instagram_users'),
                   'instagram_media': data.get('instagram_media'),
                   'title': 'home'}

    context['about_inprobes'] = config.ABOUT_INPROBES
    return render(request, 'index/home.html', {'context': context})


def single_page(request, slug):
    context = dict()

    try:
        page = Page.objects.get(slug=slug)

    except ObjectDoesNotExist:
        context['title'] = '404'
        return render(request, '404.html', {'context': context})

    context['title'] = page.header
    context['nav_key'] = slug
    return render(request, 'index/page.html',
                  {'context': context, 'page': page})


def register(request):
    context = dict()
    context['title'] = 'register'
    context['registered'] = False
    context['banner'] = _('Register here please')
    context['show_register_form'] = True

    if config.DISABLE_REGISTRATION:
        context['show_register_form'] = False
        context['banner'] = config.DISABLE_REGISTRATION_MESSAGE
        user_form = UserForm()
        user_profile_form = UserProfileForm()
    else:
        # Check the method
        if request.method == 'POST':
            user_form = UserForm(data=request.POST)
            user_profile_form = UserProfileForm(data=request.POST)

            if user_form.is_valid() and user_profile_form.is_valid():
                if User.objects.filter(
                        email=user_form.cleaned_data['email']
                ).exists():
                    # context['show_register_form'] = False
                    context['banner'] = _(
                        'User with this email address already exists')
                else:
                    user = user_form.save()
                    user.set_password(user.password)
                    user.save()

                    profile = user_profile_form.save(commit=False)
                    profile.user = user

                    # Set profile picture if user presented
                    if 'profile_pic' in request.FILES:
                        profile.profile_pic = request.FILES['profile_pic']

                    # TODO: Remove later
                    profile.level = UserProfile.MEDIUM
                    profile.save()
                    context['registered'] = True
                    context['banner'] = _(
                        'Congrate! you registered successfully :)')
                    data = redis.StrictRedis()
                    data.incr('all_users')

                    newuser = authenticate(
                        username=request.POST.get('username'),
                        password=request.POST.get('password')
                    )
                    if newuser:
                        # username and password is true, so:
                        if newuser.is_active:
                            login(request, newuser)
                            return redirect('/dashboard/')
                    return redirect('/login/?registered=True')

            else:
                context['banner'] = _("There are some errors")
                # user_form.errors, user_profile_form.errors
        else:
            user_form = UserForm()
            user_profile_form = UserProfileForm()
    return render(
        request, 'index/register.html', {
            'context': context,
            'user_form': user_form,
            'user_profile_form': user_profile_form
        }
    )


def user_login(request):
    # TODO: Add next param to url
    context = dict()
    context['title'] = _('login')
    context['nav_key'] = 'login'
    context['banner'] = _('Please enter your details below')
    _next = request.GET.get('next', '/dashboard/')

    if request.user.is_authenticated():
        try:
            RedisCache.cache(request.user)
        except:
            pass
        return redirect(_next)
    # Check the method
    if request.method == 'POST':
        # User is submitted a login form, so:
        user = False
        user_input = request.POST.get('username')
        password = request.POST.get('password')
        try:
            validate_email(user_input)
            guest = User.objects.filter(email=user_input).first()
            username = False
        except ValidationError:
            username = user_input
            guest = None

        if guest:
            user = authenticate(username=guest.username, password=password)
        elif username:
            user = authenticate(username=username, password=password)

        if user:
            # username and password is true, so:
            if user.is_active:
                login(request, user)
                try:
                    RedisCache.cache(user)
                except:
                    pass
                return redirect(_next)
            else:
                context['banner'] = _('Sorry you are not allowed')
        else:
            context['banner'] = _('wrong details')

    if request.GET.get('registered'):
        context['banner'] = _('Congrate! you registered successfully :)')

    return render(request, 'index/login.html', {'context': context})


class ServicesPageView(TemplateView):
    template_name = "index/services.html"

    def get_context_data(self, **kwargs):
        context = super(ServicesPageView, self).get_context_data(**kwargs)
        context['nav_key'] = 'services'
        return context


@login_required
def user_logout(request):
    user = request.user
    RedisCache.delete(user)
    logout(request)
    return redirect('/login/')

@login_required
def blank(request):
    return render(request, 'index/dashboard/blank.html')


@login_required
def tmp(request):
    return render(request, 'index/dashboard/tmp.html')


@login_required
def dashboard(request):
    user = request.user
    profile = user.userprofile
    packages = AccountPackage.objects.filter(enable=True)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect(reverse('complete_registration'))

    context = dict()
    instagram_id = profile.instagram_id
    media = Media.objects.select_related(
        'location', 'images', 'caption', 'user'
    ).filter(user=instagram_id).order_by('-created_time')

    media_ids = media.values_list('id', flat=True)
    tags = Tag.objects.filter(media__id__in=media_ids).annotate(
        internal_media_count=Count('media')
    ).values('name', 'internal_media_count').order_by('internal_media_count')

    tagged_users = InstagramUser.objects.filter(tagged_media__id__in=media_ids)

    len_tags = tags.count()
    if len_tags > 20:
        context['tags_bar'] = tags[len_tags - 20:]
    else:
        context['tags_bar'] = tags
    context['tags'] = len(tags)
    context['tagged_users'] = len(tagged_users)

    # context['tags'] = data.hget(user.id, 'tags')
    # context['tagged_users'] = data.hget(user.id, 'tagged_users')

    len_media = media.count()
    if len_media > 40:
        context['chart'] = media.reverse()[len_media - 40:]
    else:
        context['chart'] = media.reverse()
    # chart = data.hget(user.id, 'chart')
    # context['chart'] = deserialize(chart)

    # table = data.hget(user.id, 'table')
    # context['table'] = deserialize(table)[:5]
    context['table'] = media.order_by('-like_count', '-comment_count')[:7]

    context['ratio'] = {}
    context['ratio']['image'] = media.filter(type='image').count()
    context['ratio']['video'] = media.filter(type='video').count()
    context['ratio']['carousel'] = media.filter(type='carousel').count()
    # context['ratio'] = json.loads(data.hget(user.id, 'ratio'))

    context['like'] = {}
    context['like']['sum'] = media.aggregate(Sum('like_count')).values()[0]
    context['like']['avg'] = media.aggregate(Avg('like_count')).values()[0]
    # context['like'] = json.loads(data.hget(user.id, 'like'))
    # context['likes'] = data.hget(user.id, 'like')
    # print '****************'
    # print context['like'], type(context['like'])
    # print context['likes'], type(context['likes'])

    context['comment'] = {}
    context['comment']['sum'] = media.aggregate(Sum('comment_count')).values()[
        0]
    context['comment']['avg'] = media.aggregate(Avg('comment_count')).values()[
        0]
    # context['comment'] = json.loads(data.hget(user.id, 'comment'))

    context['relation'] = {}
    context['relation']['followers'] = profile.instagram_id.followed_by
    context['relation']['following'] = profile.instagram_id.follows
    # context['relation'] =json.loads(data.hget(user.id, 'relation'))

    context['type'] = {}
    context['type']['images'] = media.filter(type='image').count()
    context['type']['videos'] = media.filter(type='video').count()
    context['type']['carousel'] = media.filter(type='carousel').count()
    context['type']['total'] = media.count()
    # context['type'] =json.loads(data.hget(user.id, 'type'))

    created_times = media.values_list('created_time', flat=True)
    context['day_streak'] = len(
        set([created_time.strftime('%y-%m-%d') for created_time in
             created_times])
    )
    # context['day_streak'] = data.hget(user.id, 'day_streak')

    context['nav_bar_side'] = 'dashboard'
    context['analytic_permission'] = profile.has_analytic_permission()
    return render(
        request, 'index/dashboard/index.html',
        {'context': context, 'profile': profile, 'user': user,
         'packages': packages}
    )


@login_required
def post_list(request, hid=None):
    user = request.user
    profile = UserProfile.load(user.id)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = dict()
    instagram_id = profile.instagram_id
    media = Media.objects.select_related(
        'location', 'images', 'caption', 'user'
    ).filter(user=instagram_id).order_by('-created_time')

    paginator = Paginator(media, 24)

    page = request.GET.get('page')
    try:
        context['media'] = paginator.page(page)
    except PageNotAnInteger:
        context['media'] = paginator.page(1)
    except EmptyPage:
        context['media'] = paginator.page(paginator.num_pages)

    context['nav_bar_side'] = 'post_list'
    return render(request, 'index/dashboard/post_list.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def post_analytics(request, pid):
    user = request.user
    profile = user.userprofile
    insta_user = profile.instagram_id
    context = dict()
    dic = {'id': pid, 'user': insta_user}
    context['post'] = get_object_or_404(Media, **dic)
    return render(
        request, 'index/dashboard/post_detail.html',
        {'context': context, 'profile': profile, 'user': user}
    )


@login_required
def analytic(request):
    user = request.user
    profile = user.userprofile

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect(reverse('complete_registration'))

    if not profile.has_analytic_permission():
        messages.add_message(
            request, messages.INFO, _(
                'Dear user, to access complete analytics should upgrade your account'
            )
        )
        return redirect(reverse('financial'))

    context = dict()
    media = Media.objects.select_related(
        'location', 'images', 'caption', 'user'
    ).filter(user=profile.instagram_id).order_by('-created_time')

    media_ids = media.values_list('id', flat=True)
    context['tags_bar'] = Tag.objects.filter(media__id__in=media_ids).annotate(
        internal_media_count=Count('media')
    ).values('name', 'internal_media_count').order_by('internal_media_count')

    context['tagged_users'] = InstagramUser.objects.filter(
        tagged_media__id__in=media_ids
    ).annotate(
        tagged_count=Count('id')
    ).order_by('tagged_count')

    context['table'] = media.order_by('-like_count', '-comment_count')
    context['chart'] = media.reverse()

    context['most'] = {
        'commented': media.order_by('-comment_count').first(),
        'liked': media.order_by('-like_count').first()
    }

    wkd = {0: 'Saturday', 1: 'Sunday', 2: 'Monday', 3: 'Tuesday',
           4: 'Wednesday', 5: 'Thursday', 6: 'Friday'}
    wk_posts = [{'media': [m for m in media if m.created_time.weekday() == i],
                 'number': i, 'name': wkd[i]} for i in
                range(7)]

    # GET CURRENT TIMEZONE FIRST, CHANGE POST TIMES
    # IN CURRENT TIMEZONE AND CALCULATE THEM
    ctime = timezone.get_current_timezone()
    ptime = [
        {
            'range': '%s-%s' % (i * 4, (i + 1) * 4),
            'media': [m for m in media if
                      m.created_time.astimezone(ctime).hour in range(i * 4, (
                              i + 1) * 4)]} for
        i in range(6)]

    for i in wk_posts:
        i['like_count'] = CSum(i['media'], 'like_count')
        i['comment_count'] = CSum(i['media'], 'comment_count')
        try:
            i['like_avg'] = round(i['like_count'] / len(i['media']), 2)
            i['comment_avg'] = round(i['comment_count'] / len(i['media']), 2)
        except:
            i['like_avg'] = 0
            i['comment_avg'] = 0

    for i in ptime:
        i['like_count'] = CSum(i['media'], 'like_count')
        i['comment_count'] = CSum(i['media'], 'comment_count')

        try:
            i['like_avg'] = round(i['like_count'] / len(i['media']), 2)
            i['comment_avg'] = round(i['comment_count'] / len(i['media']), 2)
        except:
            i['like_avg'] = 0
            i['comment_avg'] = 0

    pt_notes = dict()
    pt_max_like = max([i['like_avg'] for i in ptime])
    pt_notes['max_like'] = {
        'ranges': [i['range'] for i in ptime if i['like_avg'] == pt_max_like],
        'avg': pt_max_like}

    pt_min_like = min([i['like_avg'] for i in ptime])
    pt_notes['min_like'] = {
        'ranges': [i['range'] for i in ptime if i['like_avg'] == pt_min_like],
        'avg': pt_min_like}

    pt_min_comment = min([i['comment_avg'] for i in ptime])
    pt_notes['min_comment'] = {'ranges': [i['range'] for i in ptime if
                                          i['comment_avg'] == pt_min_comment],
                               'avg': pt_min_comment}

    pt_max_comment = max([i['comment_avg'] for i in ptime])
    pt_notes['max_comment'] = {'ranges': [i['range'] for i in ptime if
                                          i['comment_avg'] == pt_max_comment],
                               'avg': pt_max_comment}

    pt_max_post = max([len(i['media']) for i in ptime])
    pt_notes['max_post'] = {
        'ranges': [i['range'] for i in ptime if len(i['media']) == pt_max_post],
        'avg': pt_max_post}

    pt_min_post = min([len(i['media']) for i in ptime])
    pt_notes['min_post'] = {
        'ranges': [i['range'] for i in ptime if len(i['media']) == pt_min_post],
        'avg': pt_min_post}

    context['pt_notes'] = pt_notes

    wk_notes = dict()
    wk_max_like = max([i['like_avg'] for i in wk_posts])
    wk_notes['max_like'] = {
        'days': [i['name'] for i in wk_posts if i['like_avg'] == wk_max_like],
        'avg': wk_max_like}

    wk_min_like = min([i['like_avg'] for i in wk_posts])
    wk_notes['min_like'] = {
        'days': [i['name'] for i in wk_posts if i['like_avg'] == wk_min_like],
        'avg': wk_min_like}

    wk_min_comment = min([i['comment_avg'] for i in wk_posts])
    wk_notes['min_comment'] = {'days': [i['name'] for i in wk_posts if
                                        i['comment_avg'] == wk_min_comment],
                               'avg': wk_min_comment}

    wk_max_comment = max([i['comment_avg'] for i in wk_posts])
    wk_notes['max_comment'] = {'days': [i['name'] for i in wk_posts if
                                        i['comment_avg'] == wk_max_comment],
                               'avg': wk_max_comment}

    wk_max_post = max([len(i['media']) for i in wk_posts])
    wk_notes['max_post'] = {
        'days': [i['name'] for i in wk_posts if len(i['media']) == wk_max_post],
        'avg': wk_max_post}

    wk_min_post = min([len(i['media']) for i in wk_posts])
    wk_notes['min_post'] = {
        'days': [i['name'] for i in wk_posts if len(i['media']) == wk_min_post],
        'avg': wk_min_post}

    context['wk_notes'] = wk_notes

    context['ptime'] = ptime

    context['wk_posts'] = wk_posts

    paginator = Paginator(context['table'], 20)

    page = request.GET.get('page')
    try:
        context['table'] = paginator.page(page)
    except PageNotAnInteger:
        context['table'] = paginator.page(1)
    except EmptyPage:
        context['table'] = paginator.page(paginator.num_pages)

    context['nav_bar_side'] = 'analytic'
    return render(request, 'index/dashboard/analytic.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def hashtags(request):
    user = request.user
    profile = UserProfile.load(user.id)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = dict()
    context['reports'] = profile.hashtag_reports.all()
    context['nav_bar_side'] = 'hashtags'
    return render(request, 'index/dashboard/hashtag_index.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def hashtag_posts(request, hid):
    user = request.user
    profile = UserProfile.load(user.id)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = dict()
    order_by = 'like_count'
    order_type = 'dsc'
    if request.method == "POST":
        order_by = request.POST.get('filter_key', 'like_count')
        order_type = request.POST.get('filter_type', True)

    context['filter_key'] = order_by
    context['filter_type'] = order_type

    if order_type == 'dsc':
        order_by = '-' + order_by

    hashtag = get_object_or_404(HashtagReport, id=hid)
    media = hashtag.tag.media.select_related(
        'location', 'images', 'caption', 'user'
    ).all().order_by(order_by)
    paginator = Paginator(media, 24)

    page = request.GET.get('page')
    try:
        context['media'] = paginator.page(page)
    except PageNotAnInteger:
        context['media'] = paginator.page(1)
    except EmptyPage:
        context['media'] = paginator.page(paginator.num_pages)

    context['hashtag'] = hashtag
    context['nav_bar_side'] = 'hashtags'
    return render(request, 'index/dashboard/hashtag_posts.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def create_hashtag(request):
    user = request.user
    profile = UserProfile.load(user.id)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    if not profile.can_add_hashtag():
        messages.add_message(
            request, messages.INFO,
            _('You reached maximum account hashtags, '
              'to add new hashtag upgrade please')
        )
        return redirect(reverse('financial'))
    if request.POST:
        # Create report instance
        hid = request.POST.get('hashtag')
        hashtag = get_object_or_404(Tag, id=hid)
        if hashtag.media_count > 5000:
            return HttpResponse(_("Bad request sent to the server"), status=400)

        report = HashtagReport.objects.create(user=profile, tag=hashtag)
        return redirect('hashtags')

    if request.is_ajax():
        # Update Hashtags_media count and get return
        slug = request.GET.get('hashtag')
        if len(slug.split()) > 1:
            return HttpResponse(_("Hashtag may not contain space"), status=400)
        # tag = Tag.objects.get_or_create(name=slug)
        tag, error = HashtagHandler.update_tag(profile.instagram_id, slug)
        if error:
            return HttpResponse(_("Your instagram token is expired and your are"
                                  " not allowed to create campaign"),
                                status=400)
        return JsonResponse({'count': tag.media_count, 'id': tag.id},
                            status=200)

    context = dict()
    context['nav_bar_side'] = 'hashtags'
    return render(request, 'index/dashboard/create_hashtag.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def hashtag_analytics(request, hid):
    user = request.user
    profile = UserProfile.load(user.id)

    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = dict()
    hashtag = get_object_or_404(HashtagReport, id=hid)
    media = hashtag.tag.media.select_related(
        'location', 'images', 'caption', 'user'
    )
    truncate_date = connection.ops.date_trunc_sql('day', 'created_time')
    media = media.extra({'day': truncate_date})
    context['post_by_day'] = media.values('day').annotate(
        posts=Count('pk'),
        likes_count=Sum('like_count'),
        comments_count=Sum('comment_count')
    ).order_by('day')

    context['ratio'] = dict()
    context['ratio']['image'] = media.filter(type='image').count()
    context['ratio']['video'] = media.filter(type='video').count()
    context['ratio']['carousel'] = media.filter(type='carousel').count()
    context['report'] = hashtag
    media_ids = media.values_list('id', flat=True)
    context['tags_bar'] = Tag.objects.filter(media__id__in=media_ids).annotate(
        internal_media_count=Count('media')
    ).values('name', 'internal_media_count').order_by('internal_media_count')
    context['participants'] = InstagramUser.objects.filter(
        posted_media__id__in=media_ids
    ).annotate(
        posts_count=Count('id')
    ).order_by('posts_count')

    first = media.order_by('created_time').first()
    last = media.order_by('created_time').last()
    context['day_streak'] = {
        'first': first,
        'days': (last.created_time - first.created_time).days + 1,
        'count': media.count(),
        'avg': media.count() / (
                    (last.created_time - first.created_time).days + 1)
    }
    context['tagged_users'] = InstagramUser.objects.filter(
        tagged_media__id__in=media_ids)
    context['media_locations'] = media.filter(location__isnull=False).values(
        'location')

    context['nav_bar_side'] = 'hashtags'
    return render(request, 'index/dashboard/hashtag_analytic.html',
                  {'context': context, 'profile': profile, 'user': user})


@login_required
def add_instagram_account(request):
    # This is a first step request:

    handler = InstagramHandler()
    if request.method == 'POST':
        return redirect(handler.url)

    # This is the second step
    else:
        user = request.user
        code = request.GET.get('code')
        try:
            instagram_user = handler.exchange_code_to_access_token(code)
            UserProfile.add_instagram_account(user.id, instagram_user)
            # update_user_account_task.delay(request.user.id)
        # except OAuth2AuthExchangeError:
        except Exception as e:
            # TODO : Check the error and continue registration
            pass
        return redirect('account.views.complete_registration')


# @login_required
# def support(request, ticket=None):
#     user = request.user
#     profile = UserProfile.load(user.id)
#     context = dict()
#     context['nav_bar_side'] = 'support'
#
#     # If ticket is not None we should find and show the
#     #  ticket thread and responses
#     if ticket is not None:
#         try:
#             if request.method == 'POST':
#                 # Response submited
#                 tk = Ticket.objects.get(id=ticket)
#                 rs = ResponseForm(request.POST)
#                 if rs.is_valid():
#                     response = rs.save(commit=False)
#                     response.user = request.user
#                     response.ticket = tk
#                     response.save()
#                     context['result'] = True
#                 else:
#                     context['error'] = True
#             tk = Ticket.objects.get(id=ticket)
#             if tk.user == request.user:
#                 context['ticket'] = Ticket.objects.get(id=ticket)
#                 context['responses'] = context['ticket'].response_set.all()
#                 context['response_form'] = ResponseForm()
#                 # print context['ticket'], context['responses']
#                 return render(request, 'index/dashboard/tickets.html',
#                               {'context': context, 'profile': profile,
#                                'user': user})
#         except:
#             pass
#
#     # Fetch all threads for main support page
#     context['tickets'] = Ticket.objects.all().filter(user=request.user)
#
#     if request.method == "POST":
#         tk = TicketForm(request.POST)
#         if tk.is_valid():
#             ticket = tk.save(commit=False)
#             ticket.user = request.user
#             ticket.save()
#             context['result'] = True
#         else:
#             context['error'] = True
#
#     context['form'] = TicketForm()
#     return render(request, 'index/dashboard/support.html',
#                   {'context': context, 'profile': profile, 'user': user})
#


@login_required
def financial(request):
    user = request.user
    profile = UserProfile.load(user.id)
    context = dict()
    context['nav_bar_side'] = 'financial'
    context['account_packages'] = AccountPackage.objects.filter(enable=True)
    return render(
        request, 'index/dashboard/financial.html',
        {'context': context, 'profile': profile, 'user': user}
    )


def prices(request):
    return render(
        request, "index/price.html",
        {'packages': AccountPackage.objects.filter(enable=True)}
    )


@login_required
def settings(request):
    user = request.user
    profile = UserProfile.load(user.id)
    context = dict()
    context['nav_bar_side'] = 'settings'
    return render(request, 'index/dashboard/settings.html',
                  {'context': context, 'profile': profile, 'user': user})


# @login_required
# def festival_panel(request):
#     user = request.user
#     profile = user.userprofile
#     context = dict()
#     if not profile.has_instagram or not profile.parsed or not profile.confirm:
#         return redirect('account.views.complete_registration')
#
#     if request.method == 'POST':
#         festival = FestivalForm(data=request.POST)
#         if festival.is_valid():
#             festival.save(request.user.userprofile)
#             return HttpResponse('ok')
#         return HttpResponse(festival.errors)
#     context['lives'] = Festival.live.filter(
#         conductor=request.user.userprofile
#     )
#     context['form'] = FestivalForm()
#     context['nav_bar_side'] = 'festival'
#     return render(request, 'widget/festival/index.html',
#                   {'context': context, 'profile': profile, 'user': user})


@login_required
def get_media(request):
    # update_user_account_task.delay(request.user.id)
    src = request.GET.get('next', '/dashboard')
    return redirect(src)


def test_word(request):
    test_wc()
    return HttpResponse('OK')


@user_passes_test(lambda u: u.is_staff)
def flower_view(request):
    response = HttpResponse()
    path = request.get_full_path()
    path = path.replace('flower', 'monitor', 1)
    response['X-Accel-Redirect'] = path
    return response
