from django.contrib import admin
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _

from .models import Page, UserProfile, InstagramAccounts, FacebookCredentials


class InstagramAccountsInline(admin.TabularInline):
    model = InstagramAccounts
    readonly_fields = ['date_added']
    extra = 1


class FacebookCredentialsInline(admin.TabularInline):
    model = FacebookCredentials
    readonly_fields = ['created_time', 'modified_time']
    extra = 0
    can_delete = False

    def get_queryset(self, request):
        qs = super(FacebookCredentialsInline, self).get_queryset(request)
        return qs.filter(valid=True)


class PageAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('header',), }
    list_display = ['header', 'meta_desc', 'created', 'updated']


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'avatar', 'user', 'email', 'level', 'instagram_id',
                    'bio', 'media_count', 'joined_at', 'confirm', 'parsed']
    list_filter = ['level']
    raw_id_fields = ('user', 'instagram_id',)
    search_fields = ['user__username', 'instagram_id__username']
    list_display_links = ['id', 'user', 'email']
    inlines = [InstagramAccountsInline, FacebookCredentialsInline]


admin.site.register(Page, PageAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.unregister(Group)

# Django admin panel custom configurations
admin.site.site_header = _("Inprobes Admin Panel")
admin.site.site_title = _("Inprobes Admin Panel")
admin.site.index_title = _("Inprobes Admin Panel")
