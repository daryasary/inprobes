from django import forms
from django.contrib.auth.models import User
from .models import UserProfile
from django.utils.translation import ugettext_lazy as _


class UserForm(forms.ModelForm):
    password = forms.CharField(label=_('Password'),
                               widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': _('Password')}))
    fullname = forms.CharField(label=_('Full Name'),
                               widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Full Name')}))

    # def clean(self):
    # 	password = self.cleaned_data.get('password')
    # 	confirm_password = self.cleaned_data.get('confirm_password')

    # 	if password and password != confirm_password:
    # 		raise forms.ValidationError(_("Passwords don't match, please try again"))

    # 	return self.cleaned_data

    class Meta:
        model = User
        fields = ('fullname', 'username', 'email', 'password')
        widgets = {
            # 'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            # 'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'username': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Username')}),
            'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': _('Email')}),
        }

    error_messages = {
        'password': {
            'max_length': _("Passwords don't match, please try again"),
        },
    }


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('website', 'mobile', 'profile_pic')
        widgets = {
            'website': forms.URLInput(attrs={'class': 'form-control'}),
            'mobile': forms.TextInput(attrs={'class': 'form-control'})
        }
