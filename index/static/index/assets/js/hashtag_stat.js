
/*=============================================================
    Authour URI: www.binarycart.com
    Version: 1.1
    License: MIT

    http://opensource.org/licenses/MIT

    100% To use For Personal And Commercial Use.

    ========================================================  */

(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
            METIS MENU
            ======================================*/
            $('#main-menu').metisMenu();

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });
           /*====================================
            MORRIS BAR CHART
         ======================================*/
            Morris.Bar({
                element: 'hashtag-days-bar-chart',
                data: hashtagDaysBarData,
                xkey: 'x',
                ykeys: ['y'],
                labels: ['Your usage'],
                hideHover: 'auto',
                resize: true,
                barColors:['#6ed3cf']
            });
            /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'hashtag-donut-chart',
                data: donutData,
                resize: true,
                colors: ['#ff8c2d','#cc7024', '#99541b']
            });
         /*====================================
            MORRIS BAR CHART
         ======================================*/
            Morris.Bar({
                element: 'tags-bar-chart',
                data: barData,
                xkey: 'x',
                ykeys: ['y1'],
                labels: ['Your usage'],
                hideHover: 'auto',
                resize: true,
                barColors:['#6ed3cf']
            });
            /*====================================
            MORRIS LINE CHART
         ======================================*/
            Morris.Line({
                element: 'dashboard-line-chart',
                data: lineData,
                xkey: 'm',
                ykeys: ['l', 'c'],
                labels: ['Likes', 'Comments'],
                hideHover: 'auto',
                parseTime: false,
                resize: true,
                lineColors: ['#EA2D53','#467BE9']
            });

        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));
