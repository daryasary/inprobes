from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField

from services.InstagramData.models import User as insta_user
from services.graph_api.models import Account


class Page(models.Model):
    header = models.CharField(max_length=255)
    body = RichTextField()
    meta_desc = models.TextField(
        blank=True,
        help_text="Meta description of page, max length to 160 character"
    )
    created = models.DateTimeField(auto_created=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique=True)

    class Meta:
        verbose_name = _('Page')
        verbose_name_plural = _('Pages')

    def __unicode__(self):
        return self.header

    def save(self, *args, **kwargs):
        super(Page, self).save(*args, **kwargs)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('page', args=[self.slug])


class UserProfile(models.Model):
    """Relation with django User that imported at top"""
    LOW = 'L'
    MEDIUM = 'M'
    HIGH = 'H'

    LEVELS = (
        (LOW, _('Low')),
        (MEDIUM, _('Medium')),
        (HIGH, _('High'))
    )
    user = models.OneToOneField(User)

    fullname = models.CharField(verbose_name=_('fullname'), max_length=10,
                                blank=True)
    level = models.CharField(verbose_name=_('level'), choices=LEVELS,
                             max_length=1, default='L')
    website = models.URLField(verbose_name=_('website'), blank=True)
    mobile = models.CharField(verbose_name=_('mobile'), max_length=255,
                              blank=True)
    profile_pic = models.ImageField(verbose_name=_('profile_pic'),
                                    upload_to='profile_pics', blank=True)
    instagram_id = models.ForeignKey(insta_user, blank=True, null=True)
    accounts = models.ManyToManyField(Account, through='InstagramAccounts')
    confirm = models.BooleanField(default=False)
    parsed = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def __unicode__(self, *args):
        return self.user.username

    @property
    def has_instagram(self):
        return self.instagram_id is not None

    @property
    def has_facebook(self):
        return self.facebook_tokens.filter(valid=True).exists()

    @property
    def facebook_token(self):
        token = self.facebook_tokens.filter(valid=True).last()
        if token:
            return token.access_token

    @property
    def has_instagram_account(self):
        return self.instagram_accounts.filter(status=True).exists()

    @property
    def primary_account(self):
        account = self.instagram_accounts.filter(status=True, primary=True)
        return account.first()

    def expire_facebook_token(self):
        """When api face 400 status then facebook token should be expired"""
        token = self.facebook_token.filter(valid=True).last()
        token.valid = False
        token.save()

    def make_parsed(self):
        self.parsed = True
        self.save()

    def email(self):
        return self.user.email

    def bio(self):
        if self.instagram_id:
            return self.instagram_id.bio
        else:
            return None

    def joined_at(self):
        if self.instagram_id:
            return self.instagram_id.added
        else:
            return None

    def media_count(self):
        if self.instagram_id:
            return self.instagram_id.media
        else:
            return None

    def avatar(self):
        if self.instagram_id:
            return format_html(
                '<img src="{}" style="height: 40px;width: 40px;"/>',
                self.instagram_id.profile_picture)
        else:
            return self.profile_pic

    @classmethod
    def confirm_user(cls, user_id):
        user = User.objects.get(id=user_id)
        if user:
            user.userprofile.confirm = True
            user.userprofile.save()
            return True
        return False

    @classmethod
    def set_parsed(cls, user_id):
        """Call this method when media cached and parsed for the first time"""
        user = User.objects.get(id=user_id)
        if user:
            user.userprofile.parsed = True
            user.userprofile.save()
            return True
        return False

    @classmethod
    def load(cls, user_id):
        user = User.objects.get(id=user_id)
        return user.userprofile

    @classmethod
    def add_instagram_account(cls, user_id, instagram_user):
        profile = cls.load(user_id)
        profile.instagram_id = instagram_user
        profile.save()

    def set_instageram_id_to_null(self):
        """IF handler face an access token error should remove instagram_id
        to force user for setting new instagram account"""
        self.instagram_id = None
        self.parsed = False
        self.save()

    def update_level(self, level):
        self.level = level
        self.save()

    def has_analytic_permission(self):
        purchases = self.user.purchase.filter(valid=True)
        current = any([purchase.is_valid for purchase in purchases])
        if self.instagram_id.is_free:
            return True
        if self.level == self.LOW:
            return False
        return current

    def can_add_hashtag(self):
        limit = {'H': 5, 'M': 3, 'L': 0}
        return self.hashtag_reports.filter(active=True).count() < limit[self.level]


class FacebookCredentials(models.Model):
    expires_in = models.IntegerField()
    data_access_expirations_time = models.IntegerField()
    user_id = models.BigIntegerField()
    access_token = models.TextField()
    signed_request = models.TextField()

    profile = models.ForeignKey(UserProfile, related_name='facebook_tokens')

    valid = models.BooleanField(default=True)

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{} => {}'.format(self.profile, self.valid)


class InstagramAccounts(models.Model):
    profile = models.ForeignKey(UserProfile, related_name='instagram_accounts')
    account = models.ForeignKey(Account, related_name='profile')
    date_added = models.DateTimeField(auto_now_add=True)
    status = models.BooleanField(default=False)
    primary = models.BooleanField(default=False)

    def __unicode__(self):
        return '{} > {}'.format(self.profile, self.account)

    def save(self, **kwargs):
        primary = InstagramAccounts.objects.filter(
                profile=self.profile, status=True, primary=True
        ).exists()
        self.primary = self.status and (not primary)
        return super(InstagramAccounts, self).save(**kwargs)
