from django.conf.urls import url
from django.views.generic import TemplateView

from index.views import ServicesPageView
from . import views

urlpatterns = [
    # Home pages
    url(r'^$', views.home, name='home'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='user_login'),
    url(r'^logout/$', views.user_logout, name='user_logout'),
    url(r'^services/$', ServicesPageView.as_view(), name='services'),

    # Dashboard pages
    url(r'^dashboard_old/$', views.dashboard, name='dashboard_old'),
    url(r'^blank/$', views.blank, name='blank'),
    url(r'^tmp/$', views.tmp, name='tmp'),
    url(r'^register_instagram_account/$', views.add_instagram_account,name='add_instagram_account'),
    url(r'^update_user_token/$', TemplateView.as_view(template_name="index/dashboard/update_user_token.html"), name='update_token'),
    url(r'^recent_media/$', views.get_media, name='get_media'),
    url(r'^post_list/$', views.post_list, name='post_list'),
    url(r'^post_detail/(?P<pid>.*)$', views.post_analytics, name='post_detail'),
    url(r'^hashtags/$', views.hashtags, name='hashtags'),
    # url(r'^hashtags/new', views.create_hashtag, name='create_hashtag'),
    url(r'^hashtags/(?P<hid>[0-9]+)/post_list/$', views.hashtag_posts, name='hashtags_post_list'),
    url(r'^hashtags/(?P<hid>[0-9]+)/analytics/$', views.hashtag_analytics, name='hashtags_hashtag_analytics'),
    url(r'^analytics/$', views.analytic, name='analytic'),
    # url(r'^support/$', views.support, name='support'),
    # url(r'^support/ticket/([0-9]+)/$', views.support, name='ticket'),
    # url(r'^support/$', views.support, name='support'),
    # url(r'^support/ticket/([0-9]+)/$', views.support, name='ticket'),
    url(r'^financial/$', views.financial, name='financial'),
    url(r'^settings/$', views.settings, name='settings'),
    url(r'^test_word/$', views.test_word, name='test_word'),

    # Flower proxy pass view
    url(r'^flower/', views.flower_view),

    # All other static pages
    url(r'^prices/$', views.prices, name='prices'),
    url(r'^en/terms-of-service/$', TemplateView.as_view(template_name="index/en_terms.html")),
    url(r'^en/privacy-policy/$', TemplateView.as_view(template_name="index/en_privacy.html")),
    url(r'(?P<slug>.*)/$', views.single_page, name='page'),
]
