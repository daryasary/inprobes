(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
            METIS MENU 
            ======================================*/
            $('#main-menu').metisMenu();

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });

            /*====================================
            MORRIS LINE CHART
         ======================================*/
            Morris.Line({
                element: 'dashboard-line-chart',
                data: linedata,
                xkey: 'm',
                ykeys: ['l', 'c'],
                labels: ['Likes', 'Comments'],
                hideHover: 'auto',
                parseTime: false,
                resize: true,
                lineColors: ['#EA2D53','#467BE9']
            });

            /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'dashboard-donut-chart',
                data: donutdata,
                resize: true,
                colors: ['#ff8c2d','#99541b']
            });

        
     
        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));
