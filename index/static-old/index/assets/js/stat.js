
/*=============================================================
    Authour URI: www.binarycart.com
    Version: 1.1
    License: MIT
    
    http://opensource.org/licenses/MIT

    100% To use For Personal And Commercial Use.
   
    ========================================================  */

(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {
            /*====================================
            METIS MENU 
            ======================================*/
            $('#main-menu').metisMenu();

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });
/*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-3',
                data: weekDays_posts,
                resize: true,
                colors:['#2d6f65', '#348176', '#3c9386', '#43a597', '#4bb6a7', '#5ebdb0', '#70c5b9']
            });
             /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-4',
                data: weekDays_likes,
                colors:['#b81234', '#cf153a', '#e61741', '#ea2d53', '#ec4466', '#ef5b79', '#f1738c'],
                resize: true
            });
/*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-5',
                data: weekDays_comments,
                resize: true, 
                colors: ['#1852ca', '#1b5be1', '#2f6be6', '#467be9', '#5d8bec', '#749cee', '#8aacf1']
            });
             /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-6',
                data: ptime_posts,
                resize: true, 
                colors: ['#2d6f65', '#348176', '#3c9386', '#43a597', '#4bb6a7', '#5ebdb0', '#70c5b9']
            });
                         /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-7',
                data: ptime_likes,
                resize: true,
                colors:['#b81234', '#cf153a', '#e61741', '#ea2d53', '#ec4466', '#ef5b79', '#f1738c'],
            });
                         /*====================================
          MORRIS DONUT CHART
       ======================================*/
            Morris.Donut({
                element: 'morris-donut-chart-8',
                data: ptime_comments,
                resize: true,
                colors: ['#1852ca', '#1b5be1', '#2f6be6', '#467be9', '#5d8bec', '#749cee', '#8aacf1']
            });
           
     
        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));
