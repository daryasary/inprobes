# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-12-18 20:03
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0008_userprofile_confirm'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='parsed',
            field=models.BooleanField(default=False),
        ),
    ]
