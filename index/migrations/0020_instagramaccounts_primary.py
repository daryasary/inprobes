# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2019-01-30 16:35
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('index', '0019_instagramaccounts_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='instagramaccounts',
            name='primary',
            field=models.BooleanField(default=False),
        ),
    ]
