// Submit post on submit
$('#festival-form').on('submit', function(event){
    event.preventDefault();
    console.log("form submitted!")
    var dialog = new BootstrapDialog({
            title: 'Packages',
            closable: false,
            message: function(dialogRef) {
                var $message = $('<div>Loading...</div>');
                $.ajax({
                    url: $('#festival-form').attr('action'),
                    type : "POST",
                    data: $('#festival-form').serialize(),
                    context: {
                        theDialogWeAreUsing: dialogRef
                    },
                    success: function(data) {
                        this.theDialogWeAreUsing.setMessage($($.parseHTML(data)));
                    },
                    statusCode: {
                        401: function () {
                            alert("Your session has been expired");
                        },
                        500: function () {
                            this.theDialogWeAreUsing.setMessage($('<div>There is a festival with this error</div>'));
                                }   
                            }
                        });

                return $message;
            }
        });
        dialog.updateZIndex=1050;
        dialog.realize();
        dialog.getModalHeader().hide();
        dialog.getModalFooter().hide();
        dialog.open();
});
