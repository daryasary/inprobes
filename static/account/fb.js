var loader_base = '<div class="spinner"><span class="ball-1"></span><span class="ball-2"></span><span class="ball-3"></span><span class="ball-4"></span><span class="ball-5"></span><span class="ball-6"></span><span class="ball-7"></span><span class="ball-8"></span></div>'

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
// The response object is returned with a status field that lets the
// app know the current login status of the person.
// Full docs on the response object can be found in the documentation
// for FB.getLoginStatus().
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      setCred(response);
      getInstagramAccounts(response);
    } else {
      // The person is not logged into your app or we are unable to tell.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
//    $('#registration_box').append(loader_base);
    }
}

// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
}

window.fbAsyncInit = function() {
FB.init({
  appId      : '395766774230955',
  cookie     : true,  // enable cookies to allow the server to access
                      // the session
  xfbml      : true,  // parse social plugins on this page
  version    : 'v3.2' // use graph api version 2.8
});

// Now that we've initialized the JavaScript SDK, we call
// FB.getLoginStatus().  This function gets the state of the
// person visiting this page and can return one of three states to
// the callback you provide.  They can be:
//
// 1. Logged into your app ('connected')
// 2. Logged into Facebook, but not your app ('not_authorized')
// 3. Not logged into Facebook and can't tell if they are logged into
//    your app or not.
//
// These three cases are handled in the callback function.

FB.getLoginStatus(function(response) {
  statusChangeCallback(response);
});

};

// Load the SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Here we run a very simple test of the Graph API after login is
// successful.  See statusChangeCallback() for when this call is made.
function testAPI() {
    FB.api('/me', function(response) {
//      console.log('Successful login for: ' + response.name);
      document.getElementById('status').innerHTML =
        'Thanks for logging in, ' + response.name + '!';
    });
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function setCred(response){
    $('#registration_box').empty();
    $('#msg').empty();
    $('#msg').append('لطفا منتظر بمانید...');
    $('#registration_box').append(loader_base);
    $.ajax({
       url: "/dashboard/api/set_facebook_account/",
       type: 'POST',
       data: JSON.stringify(response),
       beforeSend: function(xhr) {
           xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
       },
       success: function(result) {
       }
    });
}

function getInstagramAccounts(response){
    FB.api('/me/accounts?fields=instagram_business_account', function(response) {
        var accountsList = [];
        for (var i=0; i<response.data.length; i++){
            if(response.data[i].hasOwnProperty('instagram_business_account')){
                accountsList.push(response.data[i].instagram_business_account.id)
            }
        };
        for (var i=0; i< accountsList.length; i++){
            getAccountDetails(accountsList[i], i, accountsList.length-1)
        }
    });
}

function getAccountDetails(id, step, end){
    var url = id.toString().concat("?fields=username,name,biography,followers_count,follows_count,ig_id,media_count,profile_picture_url,website");
    FB.api(url, function(response){
        storeAccountData(response)
    })
    if (step==end){
        getAllAccounts();
    }
}

function storeAccountData(response){
    $.ajax({
       url: "/dashboard/api/save_instagram_data/",
       type: 'POST',
       data: JSON.stringify(response),
       beforeSend: function(xhr) {
           xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
       },
       success: function(result) {
       }
    });
}

function getAllAccounts(){
       $.ajax({
       url: "/dashboard/api/get_accounts/",
       type: 'GET',
       beforeSend: function(xhr) {
           xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
       },
       success: function(result){
           if (result.primary){
                window.location.href = "/dashboard/";
           }else{
               $('#registration_box').empty();
               $('#msg').empty();
               $('#msg').append('اکانت اینستاگرام اصلی خود را انتخاب کنید.');
               $('#registration_box').append('<div class="col-md-12 col-xs-12 col-sm-12" id="profiles">');
               var statusBtn;
               for (i=0; i<result.accounts.length; i++){
                   if (result.accounts[i].status){
                       statusBtn = '<button class="btn btn-default btn-feature" disabled>Selected</button>';
                   } else {
                       statusBtn = '<button onclick=submitSelectedAccount(' + result.accounts[i].set_link + ') class="btn btn-default btn-feature">Select</button>';
                   };
                   $('#profiles').append('<div class="col-md-6 col-xs-12 col-sm-12 p-5"><div class="profile-style"><div class="col-md-4 col-xs-12 col-sm-12"><img src="' +
                   result.accounts[i].account.profile_picture +
                   '" alt="" class="img-fluid img-responsive img-circle"></div><div class="col-md-8 col-xs-12 col-sm-12"><div class="m-5"><span class="size-ti" style="float: left;">' +
                   result.accounts[i].account.username +
                   '</span><span style="float: right;">' + statusBtn +
                   '</span></div><div class="col-md-12 col-xs-12 col-sm-12 mt-10" style="padding:0px;"><div class="col-md-4 col-xs-4 col-sm-4 pl-5"><b>' +
                   result.accounts[i].account.media_count +
                   '</b> Posts</div><div class="col-md-4 col-xs-4 col-sm-4 pl-5" style="padding-left:0px;"><b>' +
                   result.accounts[i].account.followers_count +
                   '</b> Followers</div><div class="col-md-4 col-xs-4 col-sm-4 pl-5" style="padding-left:20px;"><b>' +
                   result.accounts[i].account.follows_count +
                   '</b> Following</div></div><p class=" f-16" style="margin-top:60px;">' +
                   result.accounts[i].account.biography +
                   '</p><p>' +
                   result.accounts[i].account.website +
                   '</p></div></div></div>')
               }
           }
       }
    });
}

function submitSelectedAccount(id){
    var url = "/dashboard/api/select_account/".concat(id.toString()) + '/';
    $.ajax({
        url: url,
        type: "POST",
        beforeSend: function(xhr) {
           xhr.setRequestHeader("X-CSRFToken", getCookie("csrftoken"));
        },
        success: function(data) {
            window.location.href = "/dashboard/";
        },
    });
}