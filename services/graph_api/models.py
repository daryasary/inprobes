from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class Account(models.Model):
    id = models.BigIntegerField(primary_key=True)
    ig_id = models.BigIntegerField(null=True, blank=True)
    username = models.CharField(max_length=255, null=True, blank=True)
    biography = models.CharField(max_length=255, blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    profile_picture_url = models.URLField(blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)

    added = models.DateTimeField(default=timezone.now, editable=False)
    modified = models.DateTimeField(default=timezone.now, editable=False)

    media_count = models.CharField(
        max_length=50, verbose_name='Media at register time', blank=True,
        null=True
    )
    followers_count = models.CharField(
        max_length=50, verbose_name='Followers at startup', blank=True,
        null=True
    )
    follows_count = models.CharField(
        max_length=50, verbose_name='Following at startup', blank=True,
        null=True
    )
    is_business = models.BooleanField(verbose_name='Is business', default=True)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-added']
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __unicode__(self):
        return self.username


class Tag(models.Model):
    id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=64, blank=True, null=True)

    create_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        if self.name:
            return '{}({})'.format(self.name, self.id)
        return str(self.id)


class Media(models.Model):
    id = models.BigIntegerField(primary_key=True)
    ig_id = models.BigIntegerField(null=True)
    is_comment_enabled = models.NullBooleanField()
    comments_count = models.IntegerField(null=True)
    like_count = models.IntegerField(null=True)
    caption = models.TextField(blank=True)
    media_type = models.CharField(max_length=16, blank=True)
    media_url = models.URLField(blank=True)
    owner = models.ForeignKey(
        Account, blank=True, null=True, related_name='posts'
    )
    username = models.CharField(max_length=64, blank=True)
    permalink = models.URLField(blank=True)
    shortcode = models.CharField(max_length=32, blank=True)
    timestamp = models.DateTimeField(blank=True, null=True)

    hashtags = models.ManyToManyField(
        Tag, verbose_name='Hashtags', related_name='media'
    )

    partial = models.BooleanField(
        default=True, help_text=_("Check whether media crawled completely?!")
    )

    create_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return '{} ({})'.format(self.username, self.id)

    def save(self, **kwargs):
        self.partial = any(attr is None for attr in
                           [self.like_count, self.comments_count, self.caption,
                            self.media_type, self.media_url, self.username,
                            self.permalink, self.shortcode])
        return super(Media, self).save(**kwargs)
