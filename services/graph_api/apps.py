from __future__ import unicode_literals

from django.apps import AppConfig


class GraphApiConfig(AppConfig):
    name = 'graph_api'
