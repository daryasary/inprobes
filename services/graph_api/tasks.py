from celery import shared_task
from celery.schedules import crontab
from celery.task import periodic_task
from django.contrib.auth.models import User
from django.db import IntegrityError

from campaign.models import HashtagCampaign
from services.graph_api.models import Account, Media
from utils.graph_api import InstagramGraphHandler


def save_media(data):
    try:
        owner = data.pop('owner')
        account, result = Account.objects.get_or_create(**owner)
        id = data.pop('id')
        media, result = Media.objects.update_or_create(
            id=id, owner=account, defaults=data
        )
    except IntegrityError:
        data['owner'] = account
        media, result = Media.objects.update_or_create(id=id, defaults=data)
    except KeyError:
        id = data.pop('id')
        media, result = Media.objects.update_or_create(id=id, defaults=data)
    except Exception as e:
        return None
    return media


@shared_task(name="Graph media updater")
def graph_media_updater(user_id):
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return 'User #{} not exists'.format(user_id)
    else:
        if user.userprofile.primary_account is None:
            return 'User #{} has no primary account'.format(user_id)

        handler = InstagramGraphHandler(
            access_token=user.userprofile.facebook_token,
            account_id=user.userprofile.primary_account.account.id
        )
        media_list = handler.account_media_list.get()
        # TODO: When check 400 error code from graph_api library uncomment this
        # if handler.account_media_list.has_error:
        #     user.userprofile.expire_facebook_token()
        #     if user.userprofile.has_facebook:
        #         graph_media_updater(user_id)
        while handler.account_media_list.graph.has_next():
            media_list.extend(handler.account_media_list.get())

        for media_data in media_list:
            save_media(media_data)

        # Just make account parsed after first try of getting data from facebook
        user.userprofile.make_parsed()
        return "Done successfully"


@shared_task(name="Account insights updater")
def account_insight_updater(user_id):
    try:
        user = User.objects.get(id=user_id)
    except User.DoesNotExist:
        return 'User #{} not exists'.format(user_id)
    else:
        if user.userprofile.primary_account is None:
            return 'User #{} has no primary account'.format(user_id)
        handler = InstagramGraphHandler(
            access_token=user.userprofile.facebook_token,
            account_id=user.userprofile.primary_account.account.id
        )

        day_before = 0
        while day_before < 30 and handler.account_insights.graph.has_previous():
            handler.account_insights.get(reverse=True)


@shared_task(name="Hashtag campaign handler")
def hashtag_campaign_handler(campaign_id):
    try:
        campaign = HashtagCampaign.objects.get(pk=campaign_id)
    except HashtagCampaign.DoesNotExist:
        return "Campaign does not exist"
    else:
        if not campaign.active:
            return "Campaign is not active right now"

        handler = InstagramGraphHandler(
            access_token=campaign.user.facebook_token,
            hashtag_id=campaign.tag.id,
            account_id=campaign.account.id,
        )
        media_list = handler.hashtag_top_media.get()
        while handler.hashtag_recent_media.graph.has_next():
            media_list.extend(handler.hashtag_recent_media.get())

        saved_media = list()
        for media_data in media_list:
            saved_media.append(save_media(media_data))

        campaign.tag.media.add(*saved_media)

        return 'Crawled successfully'


@periodic_task(
    run_every=(crontab(minute=40, hour='0,12,18')),
    name="Graph media handler(periodic)", ignore_result=True
)
def graph_media_handler():
    accounts = Account.objects.filter(
        profile__status=True, profile__primary=True
    ).distinct()
    for account in accounts:
        instagram_account = account.profile.filter(status=True).first()
        graph_media_updater.delay(instagram_account.profile.user_id)
