from django.contrib import admin

from services.graph_api.models import Account, Media, Tag


class AccountAdmin(admin.ModelAdmin):
    list_display = ['id', 'ig_id', 'username', 'name', 'website',
                    'followers_count', 'follows_count']


class MediaAdmin(admin.ModelAdmin):
    list_display = ['id', 'partial', 'ig_id', 'comments_count', 'like_count', 'media_url',
                    'media_type', 'owner', 'permalink', 'timestamp']


admin.site.register(Account, AccountAdmin)
admin.site.register(Media, MediaAdmin)
admin.site.register(Tag)
