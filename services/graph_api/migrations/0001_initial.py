# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2019-01-30 18:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('ig_id', models.BigIntegerField(blank=True, null=True)),
                ('username', models.CharField(blank=True, max_length=255, null=True)),
                ('biography', models.CharField(blank=True, max_length=255, null=True)),
                ('website', models.URLField(blank=True, null=True)),
                ('profile_picture_url', models.URLField(blank=True, null=True)),
                ('name', models.CharField(blank=True, max_length=255, null=True)),
                ('added', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('modified', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('media_count', models.CharField(blank=True, max_length=50, null=True, verbose_name='Media at register time')),
                ('followers_count', models.CharField(blank=True, max_length=50, null=True, verbose_name='Followers at startup')),
                ('follows_count', models.CharField(blank=True, max_length=50, null=True, verbose_name='Following at startup')),
                ('is_business', models.BooleanField(default=True, verbose_name='Is business')),
                ('last_update', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['-added'],
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('caption', models.TextField(blank=True)),
                ('comments_count', models.IntegerField(blank=True)),
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('ig_id', models.BigIntegerField(blank=True)),
                ('is_comment_enabled', models.NullBooleanField()),
                ('like_count', models.IntegerField(blank=True)),
                ('media_type', models.CharField(blank=True, max_length=8)),
                ('media_url', models.URLField(blank=True)),
                ('permalink', models.URLField(blank=True)),
                ('shortcode', models.CharField(blank=True, max_length=32)),
                ('timestamp', models.DateTimeField(blank=True)),
                ('username', models.CharField(blank=True, max_length=64)),
                ('owner', models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='posts', to='graph_api.Account')),
            ],
        ),
    ]
