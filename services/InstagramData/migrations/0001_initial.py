# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-19 15:10
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('text', models.TextField()),
                ('created_at', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('low_resolution', models.URLField()),
                ('thumbnail', models.URLField()),
                ('standard_resolution', models.URLField()),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('latitude', models.CharField(blank=True, max_length=50, null=True)),
                ('longitude', models.CharField(blank=True, max_length=50, null=True)),
                ('name', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Media',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('insta_id', models.CharField(max_length=255, null=True)),
                ('attribution', models.CharField(blank=True, max_length=50, null=True)),
                ('type', models.CharField(max_length=50)),
                ('filter', models.CharField(blank=True, max_length=50, null=True)),
                ('created_time', models.DateTimeField()),
                ('link', models.URLField()),
                ('like_count', models.IntegerField(blank=True, null=True, verbose_name='likes')),
                ('user_has_liked', models.BooleanField(default=False)),
                ('comment_count', models.IntegerField(blank=True, null=True, verbose_name='Comments count')),
                ('caption', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Media_Caption', to='InstagramData.Comment')),
                ('images', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='media_images', to='InstagramData.Image')),
                ('location', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='media_location', to='InstagramData.Location')),
            ],
            options={
                'ordering': ['-created_time'],
                'verbose_name_plural': 'Media',
            },
        ),
        migrations.CreateModel(
            name='Media_Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Comment_relation_Media', to='InstagramData.Comment')),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Media_relation_Comment', to='InstagramData.Media')),
            ],
        ),
        migrations.CreateModel(
            name='Media_Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('media', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='Media_relation_Tag', to='InstagramData.Media')),
            ],
        ),
        migrations.CreateModel(
            name='Media_Users_In',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('media', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Media_users_in', to='InstagramData.Media')),
            ],
            options={
                'verbose_name_plural': 'Users in media',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('media_count', models.IntegerField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.BigIntegerField(primary_key=True, serialize=False)),
                ('username', models.CharField(max_length=255)),
                ('bio', models.CharField(blank=True, max_length=255, null=True)),
                ('website', models.URLField(blank=True, null=True)),
                ('profile_picture', models.URLField(blank=True, null=True)),
                ('full_name', models.CharField(blank=True, max_length=255, null=True)),
                ('access_token', models.CharField(blank=True, max_length=255, null=True)),
                ('added', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('modified', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('media', models.CharField(blank=True, max_length=50, null=True, verbose_name='Media at register time')),
                ('followed_by', models.CharField(blank=True, max_length=50, null=True, verbose_name='Followers at startup')),
                ('follows', models.CharField(blank=True, max_length=50, null=True, verbose_name='Following at startup')),
                ('last_update', models.DateTimeField(default=django.utils.timezone.now)),
            ],
            options={
                'ordering': ['added'],
            },
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('low_resolution', models.URLField()),
                ('low_bandwidth', models.URLField()),
                ('standard_resolution', models.URLField()),
            ],
        ),
        migrations.AddField(
            model_name='media_users_in',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Users_in_media', to='InstagramData.User'),
        ),
        migrations.AddField(
            model_name='media_tag',
            name='tag',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='Tag_relation_Media', to='InstagramData.Tag'),
        ),
        migrations.AddField(
            model_name='media',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='user_media', to='InstagramData.User'),
        ),
        migrations.AddField(
            model_name='comment',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='InstagramData.User'),
        ),
    ]
