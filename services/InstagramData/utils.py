# coding=utf-8
import redis
from instagram import InstagramAPIError
from instagram import InstagramClientError
from .models import User, Tag, Location, Comment, Image, Media, Video
from home.private_setting import API_CONFIG, FULL_PERMISSIONS

from django.utils import timezone
from instagram import client


class InstagramHandler(object):
    """"Default instagram API handler of site used to register users"""

    handler = client.InstagramAPI(**API_CONFIG)
    url = handler.get_authorize_url(FULL_PERMISSIONS)

    def __init__(self, user=None, token=None):
        if user:
            self.user = user
        self.handler.access_token = token
        super(InstagramHandler, self).__init__()

    def exchange_code_to_access_token(self, code):
        """"Exchange code to access token"""
        access_token, info = self.handler.exchange_code_for_access_token(code)
        user = self.add_user(info, access_token)
        return user

    # TODO: Refresh user access token

    def add_user(self, dic, token=None):
        """"Call this when user registered"""
        if token:
            dic['access_token'] = token
            self.handler.access_token = token
            user = self.handler.user()
            dic['follows'] = user.counts['follows']
            dic['followed_by'] = user.counts['followed_by']
            dic['media'] = user.counts['media']
        self.user = User.add_or_update_user(dic)
        return self.user

    def update_user(self):
        """Update User basic data"""
        if self.handler.access_token is None:
            self.handler.access_token = self.user.access_token
        update = self.handler.user()
        dic = self.user.__dict__
        dic['follows'] = update.counts['follows']
        dic['followed_by'] = update.counts['followed_by']
        dic['media'] = update.counts['media']
        self.user = User.add_or_update_user(dic)
        return self.user

    def update_hashtag_data(self, hashtag):
        tag = self.handler.tag(hashtag)
        tag_list = self._save_tag([tag])
        return tag_list[0]

    def update_media_comments(self, media_id):
        comments = self.handler.media_comments(media_id=media_id)
        return self._save_comments(comments, media_id)

    def get_following(self):
        """"Get the user that account is following"""
        following, _next = self.handler.user_follows()
        while _next:
            next_following = self.handler.user_follows(with_next_url=_next)
            following.extend(next_following)
        # TODO.Save User Relationships in database to use in future
        # self.save_following_users_relation(following)
        return len(following)

    def get_followers(self):
        """"Get the user that account is following"""
        followers, _next = self.handler.user_followed_by()
        while _next:
            next_following = self.handler.user_followed_by(with_next_url=_next)
            followers.extend(next_following)
        # TODO.Save User followers and relationship
        # self.save_followers_relation(followers)
        return len(followers)

    def get_user_recent_media(self):
        """Fetch user's media from instagram and pars theme"""
        media, _next = self.handler.user_recent_media(count=50)

        while _next:
            next_media, _next = self.handler.user_recent_media(
                with_next_url=_next)
            media.extend(next_media)

        self._save_media(media, user=self.user)
        return True

    def get_recent_media_for_hashtag(self, hashtag, start=None, end=None):
        medias, _next = self.handler.tag_recent_media(tag_name=hashtag.name, count=30)
        fetch = True
        print len(medias)
        # if start and end:
        #     for media in medias:
        #         if media.created_time < start or media.created_time > end:
        #             medias.pop(medias.index(media))
        #             fetch = False

        while _next and fetch:
            next_media, _next = self.handler.tag_recent_media(
                tag_name=hashtag,
                with_next_url=_next
            )
            medias.extend(next_media)
            print len(medias)

            # if start and end:
            #     for media in next_media:
            #         if media.created_time < start or media.created_time > end:
            #             medias.pop(medias.index(media))
            #             fetch = False
        medias = self._save_media(list(set(medias)), tag=hashtag)
        print len(medias)
        return medias

    def _save_media(self, all_media, user=None, tag=None):
        """Saved grabbed Media From instagram"""
        new_media_list = []

        # for media in all_media:
        while all_media:
            # print '##### Remain media: %s'%len(all_media)
            media = all_media.pop()
            tags = set()
            users_in = []
            dic = media.__dict__

            if tag is not None:
                tags.add(tag)

            # ** SAVING ADDITIONAL TAGS **
            if media.tags:
                tags.update(self._save_tag(media.tags))
            del dic['tags']

            # ** SAVING USERS_IN PHOTO **
            if media.users_in_photo:
                for u in media.users_in_photo:
                    try:
                        users_in.append(
                            User.add_or_update_user(u.user.__dict__))
                    except:
                        pass

            del dic['users_in_photo']

            # ## SAVING IMAGES ##
            images_url = dict()
            for key in media.images:
                images_url[key] = media.images[key].url
            dic['images'] = Image.save_image(images_url)

            # ** SAVING COMMENTS **
            # comments will not receive yet
            # (in sandbox mode)
            if dic['comments'] is not None:
                del dic['comments']
            # TODO: Add media comment saving
            # comments = self._get_media_comments(media.id)

            # SAVING LOCATION #
            if 'location' in dic:
                dic['location'] = self._save_location(media.location)

            # Replace instagram.models with InstagramData.models
            dic['user'] = user or self.add_user(dic['user'].__dict__)

            # SAVING CAPTION
            if dic['caption']:
                # dic['caption'] = media.caption.text
                dic['caption'] = self._save_caption(media.caption)

            if 'videos' in dic:
                dic['videos'] = self._save_video(media.videos)

            dic['insta_id'] = dic['id']
            del dic['likes'], dic['id']

            media, result = Media.objects.update_or_create(
                insta_id=dic['insta_id'], defaults=dic)
            # if result:
            #     data = redis.StrictRedis()
            #     data.incr('instagram_media')
            new_media_list.append(media)

            if tags:
                # Check media contains tag or not
                # self._save_media_tag(media, tags)
                media.hashtags.add(*tags)

            if users_in:
                # self._save_users_in_media(media, users_in)
                media.tagged_users.add(*users_in)
        return new_media_list

    @staticmethod
    def _save_tag(tag_list):
        """Save All tags inside media"""
        c_tag_list = []
        for t in tag_list:
            tag = Tag.add_or_update(t.name, t.__dict__)
            c_tag_list.append(tag)
        return c_tag_list

    @staticmethod
    def _save_location(place):
        """Get location name and points in a dic and return location instance"""
        dic = {'id': place.id, 'name': place.name,
               'latitude': place.point.latitude,
               'longitude': place.point.longitude}
        # dic['point'] = place.point.__dict__
        # location, result = Location.objects.update_or_create(id=dic['id'], name=dic['name'], defaults=dic['point'])
        location = Location.add_or_update(dic)
        return location

    @staticmethod
    def _save_video(videos):
        """Save Video of media, if it contains."""
        dic = dict()
        for q in videos:
            dic[q] = videos[q].url
        video, result = Video.objects.get_or_create(**dic)
        return video

    @staticmethod
    def _save_comments(comments, media_id):
        comments_list = list()
        for comment in comments:
            user = User.add_or_update_user(comment.user.__dict__)
            defaults = {
                'text': comment.text,
                'user': user,
                'created_at': comment.created_at
            }
            cm, rs = Comment.objects.update_or_create(
                    id=comment.id, defaults=defaults
                )
            comments_list.append(cm)

        try:
            media = Media.objects.get(insta_id=media_id)
        except:
            pass
        else:
            media.comments.add(*comments_list)
        return comments_list

    def _save_caption(self, caption):
        d = caption.__dict__
        d['user'] = self.user
        caption = Comment.store(caption.id, d)
        return caption


class AccountHandler(InstagramHandler):
    """Handler for manage account activity, inherits form InstagramHandler"""

    @classmethod
    def update_basic_account(cls, user):
        """Tool for updating basic data related to user"""
        updater = cls(user=user, token=user.access_token)
        try:
            media = updater.get_user_recent_media()
            user = updater.update_user()
            updater.user.change_last_update(timezone.now())
            return user and media
        except InstagramAPIError:
            return "Instagram API Error"
        except InstagramClientError:
            return "Instagram Client Error"

    @classmethod
    def update_advanced_account(cls, user):
        """Tool for updating advanced data related to user"""
        updater = cls(user=user, token=user.access_token)
        try:
            media = updater.get_user_recent_media()
            user = updater.update_user()
            updater.user.change_last_update(timezone.now())
            return user and media
        except InstagramAPIError:
            return "Instagram API Error"
        except InstagramClientError:
            return "Instagram Client Error"

    @classmethod
    def update_professional_account(cls, user):
        """Tool for updating professional data related to user"""
        updater = cls(user=user, token=user.access_token)
        try:
            media = updater.get_user_recent_media()
            user = updater.update_user()
            updater.user.change_last_update(timezone.now())
            return user and media
        except InstagramAPIError:
            return "Instagram API Error"
        except InstagramClientError:
            return "Instagram Client Error"


class FestivalHandler(InstagramHandler):
    @staticmethod
    def assign_medias_to_festival(medias, festival):
        festival.medias.add(*medias)
        return True

    @classmethod
    def update_results(cls, festival):
        user = festival.conductor.instagram_id
        tag = festival.hashtag
        start = festival.start_date.replace(tzinfo=None)
        end = festival.end_date.replace(tzinfo=None)
        updater = cls(user=user, token=user.access_token)
        hashtag = updater.update_hashtag_data(tag)
        medias = updater.get_recent_media_for_hashtag(hashtag, start, end)
        result = updater.assign_medias_to_festival(medias, festival)
        return result


class HashtagHandler(InstagramHandler):
    """Handler for manage hashtag activity, inherits form InstagramHandler"""

    @classmethod
    def update_media(cls, user, tag):
        """Tool for updating basic data related to user"""
        updater = cls(user=user, token=user.access_token)
        try:
            hashtag = updater.update_hashtag_data(tag)
            media = updater.get_recent_media_for_hashtag(hashtag)
            return hashtag, media
        except InstagramAPIError:
            return "Instagram API Error", None
        except InstagramClientError:
            return "Instagram Client Error", None

    @classmethod
    def update_tag(cls, user, tag):
        """Tool for updating given hashtag data"""
        updater = cls(user=user, token=user.access_token)
        try:
            hashtag = updater.update_hashtag_data(tag)
            return hashtag, False
        except InstagramAPIError:
            return "Instagram API Error", True
        except InstagramClientError:
            return "Instagram Client Error", True


class CommentHandler(InstagramHandler):
    @classmethod
    def get_comments(cls, user, media):
        updater = cls(user=user, token=user.access_token)
        try:
            comments = updater.update_media_comments(media.insta_id)
            return comments, True
        except InstagramAPIError:
            return "Instagram API Error", False
        except InstagramClientError:
            return "Instagram Client Error", False
