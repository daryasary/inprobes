from django.contrib import admin
from django.template.defaultfilters import truncatechars

from .models import User, Tag, Location, Comment, Image, Media, Video


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'bio', 'access_token', 'media', 'followed_by',
                    'follows', 'added', 'modified']
    search_fields = ['username', 'access_token']


class TagAdmin(admin.ModelAdmin):
    list_display = ['name', 'media_count']


class LocationAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'latitude', 'longitude']


class ImageAdmin(admin.ModelAdmin):
    list_display = ['id', 'image']
    readonly_fields = ['image', ]


class VideoAdmin(admin.ModelAdmin):
    list_display = ['id', 'low_resolution']


class CommentAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'text', 'created_at']


class MediaAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'type', 'location', 'link', 'like_count',
                    'caption_display', 'user_has_liked']

    def caption_display(self, obj):
        return truncatechars(obj.caption, 80)
    caption_display.short_description = 'caption'


# Register your models here.
admin.site.register(User, UserAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Image, ImageAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Media, MediaAdmin)
