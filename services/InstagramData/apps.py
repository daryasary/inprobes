from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InstagramdataConfig(AppConfig):
    name = 'InstagramData'
    verbose_name = _("Instagram data")
    verbose_name_plural = _("Instagram data")
