from __future__ import unicode_literals

import pytz
from constance import config
from django.db import models
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


class User(models.Model):
    id = models.BigIntegerField(primary_key=True)
    username = models.CharField(max_length=255)
    bio = models.CharField(max_length=255, blank=True, null=True)
    website = models.URLField(blank=True, null=True)
    profile_picture = models.URLField(blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)

    access_token = models.CharField(max_length=255, blank=True, null=True)
    added = models.DateTimeField(default=timezone.now, editable=False)
    modified = models.DateTimeField(default=timezone.now, editable=False)

    media = models.CharField(max_length=50,
                             verbose_name='Media at register time', blank=True,
                             null=True)
    followed_by = models.CharField(max_length=50,
                                   verbose_name='Followers at startup',
                                   blank=True, null=True)
    follows = models.CharField(max_length=50,
                               verbose_name='Following at startup', blank=True,
                               null=True)
    is_business = models.BooleanField(verbose_name='Is business', default=False)
    last_update = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['-added']
        verbose_name = _('User')
        verbose_name_plural = _('Users')

    def __unicode__(self):
        return self.username

    @property
    def is_free(self):
        return int(self.followed_by) < config.INSTAGRAM_FREE_LEVEL_MAX_FOLLOWERS

    def change_last_update(self, time):
        self.last_update = time
        self.save()

    @classmethod
    def add_or_update_user(cls, data):
        id = data.pop('id')
        user, result = cls.objects.update_or_create(id=id, defaults=data)
        return user


class Tag(models.Model):
    name = models.CharField(max_length=255)
    media_count = models.IntegerField(null=True, blank=True)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __unicode__(self):
        return self.name

    @classmethod
    def add_or_update(cls, name, dic):
        del dic['name']
        tag, result = Tag.objects.update_or_create(name=name, defaults=dic)
        return tag


class Location(models.Model):
    id = models.BigIntegerField(primary_key=True)
    latitude = models.CharField(max_length=50, blank=True, null=True)
    longitude = models.CharField(max_length=50, blank=True, null=True)
    name = models.CharField(max_length=255)

    class Meta:
        verbose_name = _('Location')
        verbose_name_plural = _('Locations')

    def __unicode__(self):
        return self.name

    @classmethod
    def add_or_update(cls, data):
        id = data['id']
        del data['id']
        location, result = cls.objects.update_or_create(id=id, defaults=data)
        return location


class Comment(models.Model):
    id = models.BigIntegerField(primary_key=True)
    user = models.ForeignKey(User)
    text = models.TextField()
    created_at = models.DateTimeField()

    class Meta:
        verbose_name = _('Comment')
        verbose_name_plural = _('Comments')

    def __unicode__(self):
        return '%s said : %s' % (unicode(self.user), self.text)

    def save(self, *args, **kwargs):
        self.created_at = pytz.utc.localize(self.created_at)
        super(Comment, self).save(*args, **kwargs)

    @classmethod
    def store(cls, id, data):
        del data['id']
        comment, result = cls.objects.update_or_create(id=id, defaults=data)
        return comment


class Image(models.Model):
    low_resolution = models.URLField()
    thumbnail = models.URLField()
    standard_resolution = models.URLField()

    class Meta:
        verbose_name = _('Image')
        verbose_name_plural = _('Images')

    def __unicode__(self):
        return unicode(self.id)

    @classmethod
    def save_image(cls, dic):
        image, result = Image.objects.get_or_create(**dic)
        return image

    def image(self):
        return format_html('<img src="{}" style="height: 50px;width: 50px;"/>',
                           self.thumbnail)


class Video(models.Model):
    low_resolution = models.URLField()
    low_bandwidth = models.URLField()
    standard_resolution = models.URLField()

    class Meta:
        verbose_name = _('Video')
        verbose_name_plural = _('Videos')

    def __unicode__(self):
        return unicode(self.id)


class Media(models.Model):
    insta_id = models.CharField(max_length=255, null=True)
    attribution = models.CharField(max_length=50, null=True, blank=True)
    type = models.CharField(max_length=50)
    location = models.ForeignKey(Location, related_name='media_location',
                                 null=True, blank=True)
    filter = models.CharField(max_length=50, null=True, blank=True)
    created_time = models.DateTimeField()
    link = models.URLField()
    like_count = models.IntegerField(null=True, blank=True,
                                     verbose_name='likes')
    images = models.ForeignKey(Image, related_name='media_images')
    caption = models.ForeignKey(Comment, related_name='Media_Caption',
                                blank=True, null=True)
    videos = models.ForeignKey(Video, verbose_name='media_videos', blank=True,
                               null=True)
    user_has_liked = models.BooleanField(default=False)
    user = models.ForeignKey(User, related_name='posted_media')
    comment_count = models.IntegerField(verbose_name='Comments count',
                                        blank=True, null=True)
    hashtags = models.ManyToManyField(Tag, verbose_name='Hashtags',
                                      related_name='media')
    comments = models.ManyToManyField(Comment, verbose_name='Comments',
                                      related_name='media')
    tagged_users = models.ManyToManyField(User, verbose_name='Tagged users',
                                          related_name='tagged_media')

    imported_at = models.DateTimeField(auto_now=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Media')
        verbose_name_plural = _('Medias')

    def __unicode__(self):
        return unicode(self.id)

    def show_caption(self):
        return self.caption.text

    def save(self, *args, **kwargs):
        self.created_time = pytz.utc.localize(self.created_time)
        super(Media, self).save(*args, **kwargs)
