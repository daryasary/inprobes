from django.contrib import admin
from models import Gateway, Payment


class GatewayAdmin(admin.ModelAdmin):
    list_display = ['title', 'type', 'active']


class PaymentAdmin(admin.ModelAdmin):
    list_display = [
        'user', 'amount', 'email', 'is_paid', 'ref_id', 'authority',
        'gateway', 'created_at', 'invoice_number'
    ]
    search_fields = ['user__username', 'amount', 'email', 'ref_id', 'authority']
    list_filter = ['gateway', 'is_paid']

admin.site.register(Gateway, GatewayAdmin)
admin.site.register(Payment, PaymentAdmin)
