from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils.translation import ugettext_lazy as _

from suds.client import Client

from financial.models import AccountPurchase
from payment.models import Payment, Gateway


def check_zarinpal_payment(request):
    try:
        zarinpal = Gateway.objects.get(type=Gateway.ZARINPAL, active=True)
    except:
        raise Http404
    client = Client(zarinpal.webservice)
    if request.GET.get('Status') == 'OK':
        payment = Payment.objects.get(authority=request.GET['Authority'])
        result = client.service.PaymentVerification(
            zarinpal.merchant_id,
            payment.authority,
            payment.amount
        )

        if result.Status in [100, 101]:
            payment.checked(result.RefID)
            messages.add_message(request, messages.INFO, _(
                'Payment was successful'))

        else:
            messages.add_message(request, messages.warning, _(
                'A problem occurred, try again later please.'))
        return redirect(payment)
    raise Http404


@login_required
def payment_index(request, invoice_number):
    context = dict()
    try:
        purchase = AccountPurchase.objects.select_related(
            'payment',
        ).get(payment__invoice_number=invoice_number)
    except:
        raise Http404
    else:
        context['purchase'] = purchase
        context['gateways'] = Gateway.objects.filter(active=True)
    return render(
        request, 'payment/index.html',
        {
            'context': context,
            'profile': request.user.userprofile,
            'user': request.user
        }
    )


@login_required
def connect_to_gateway(request, invoice_number, gid):
    payment = get_object_or_404(Payment, invoice_number=invoice_number)
    gateway = get_object_or_404(Gateway, pk=gid)

    client = Client(gateway.webservice)
    result = client.service.PaymentRequest(gateway.merchant_id,
                                           payment.amount,
                                           payment.purchase.package.title,
                                           payment.purchase.package.payment_description,
                                           payment.email,
                                           gateway.callback)
    if result.Status == 100:
        payment.gateway = gateway
        payment.authority = result.Authority
        payment.save()
        return redirect(
            'https://www.zarinpal.com/pg/StartPay/' + result.Authority
        )
    else:
        messages.add_message(
            request, messages.WARNING,
            _('Connection to Zarrin pal not initiated.')
        )
        return redirect(
            reverse('payment_index', args=[payment.invoice_number])
        )
