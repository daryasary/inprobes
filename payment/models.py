from __future__ import unicode_literals
import uuid
from datetime import timedelta

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Gateway(models.Model):
    ZARINPAL = 1
    GATEWAY_TYPES = (
        (ZARINPAL, "ZARINPAL"),
    )
    active = models.BooleanField(default=True)
    title = models.CharField(_("title"), max_length=255)
    logo = models.ImageField(upload_to='gateways/logo/', null=True)
    type = models.IntegerField(default=ZARINPAL, choices=GATEWAY_TYPES)
    webservice = models.CharField(max_length=128, null=True, blank=True)
    merchant_id = models.CharField(max_length=128, null=True, blank=True)
    merchant_pass = models.CharField(max_length=128, null=True, blank=True)
    payment_url = models.CharField(max_length=128, null=True, blank=True)
    callback = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        verbose_name = _('Gateway')
        verbose_name_plural = _('Gateways')

    def __str__(self):
        return self.get_type_display()


class Payment(models.Model):
    user = models.ForeignKey(User)
    amount = models.IntegerField(_('Amount'))
    description = models.TextField(_('Description'), null=True)
    email = models.EmailField(_('Email'))
    is_paid = models.BooleanField(_('Is paid'), default=False)
    ref_id = models.CharField(_('Ref ID'), max_length=64, unique=True,
                              null=True, blank=True)
    authority = models.CharField(_('Authority'), max_length=48, unique=True,
                                 null=True)
    created_at = models.DateTimeField(_('Created at'), auto_now_add=True)
    modified_at = models.DateTimeField(_('Modified at'), auto_now=True)
    invoice_number = models.UUIDField(verbose_name=_("invoice number"),
                                      unique=True, default=uuid.uuid4)
    gateway = models.ForeignKey(Gateway, null=True, related_name='payments')

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    def __init__(self, *args, **kwargs):
        super(Payment, self).__init__(*args, **kwargs)
        self._is_paid = self.is_paid

    def checked(self, ref_id):
        self.is_paid = True
        self.ref_id = ref_id
        # self.purchase.validate()
        self.save()

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'payment_index',
            kwargs={'invoice_number': self.invoice_number}
        )

    def __unicode__(self):
        return str(self.invoice_number)
