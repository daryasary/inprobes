from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^checkpayment/completedZarinpal/$', check_zarinpal_payment, name='check_payment'),
    url(r'^checkpayment/$', check_zarinpal_payment, name='check_zpal'),
    url(r'^invoice/(?P<invoice_number>[^/]+)/$', payment_index, name='payment_index'),
    url(r'^connect_to_gateway/(?P<invoice_number>[^/]+)/(?P<gid>[0-9]+)$', connect_to_gateway, name='connect_to_gateway'),
]
