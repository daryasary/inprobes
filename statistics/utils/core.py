from django.core import serializers

def deserialize(json_str):
	temp = serializers.deserialize('json', json_str)
	try:
		obj_list = [t.object for t in temp]
		return obj_list
	except:
		return None

def CSum(items, value):
	temp = 0
	for i in items:
		temp+=i.__getattribute__(value)
	return temp