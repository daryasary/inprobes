from django.db.models import Sum, Avg, Count
from django.core.serializers.json import DjangoJSONEncoder
from django.core import serializers
from django.utils import timezone

from services.InstagramData.models import Media

from statistics.utils.core import deserialize, CSum

import redis
import json


class RedisCache():
    """Redis backend that used for storing users data in redis"""

    connection = redis.StrictRedis()

    def __init__(self, user):
        if user.userprofile.instagram_id:
            self.user = user.userprofile.instagram_id
        else:
            self.user = user

    @classmethod
    def cache(cls, site_user, *args, **kwargs):
        user = cls(site_user)
        media = Media.objects.select_related('location', 'images', 'caption', 'user').filter(user=user.user)
        user._cache_dashboard(media)
        user._cache_analytics(media)

    def _cache_dashboard(self, media, *args, **kwargs):

        tags = set()
        tagged_users = set()

        for m in media:
            tags.update(m.hashtags.all())
            tags.update(m.tagged_users.all())
        # tags = [tag for m in media for tag in Media_Tag.objects.filter(media=m)]
        # tagged_users = [user for m in media for user in Media_Users_In.objects.filter(media=m)]
        # day_streak = len(media.values('created_time').annotate(the_count = Count('created_time')))
        day_streak = len(set([m.created_time.strftime('%d-%m-%d') for m in media]))
        self.save('tags', len(tags))
        self.save('tagged_users', len(tagged_users))
        self.save('day_streak', day_streak)

        chart = self.serialize_obj(media.reverse())
        self.save('chart', chart)

        # table = self.serialize_obj(media.order_by('-like_count', '-comment_count')[:5])
        table = self.serialize_obj(media.order_by('-like_count', '-comment_count'))
        self.save('table', table)

        ratio = dict()
        ratio['image'] = media.filter(type='image').count()
        ratio['video'] = media.filter(type='video').count()
        self.save('ratio', json.dumps(ratio))

        like = dict()
        like['sum'] = media.aggregate(Sum('like_count')).values()[0]
        like['avg'] = media.aggregate(Avg('like_count')).values()[0]
        like = json.dumps(like)
        self.save('like', like)

        comment = dict()
        comment['sum'] = media.aggregate(Sum('comment_count')).values()[0]
        comment['avg'] = media.aggregate(Avg('comment_count')).values()[0]
        self.save('comment', json.dumps(comment))

        relation = dict()
        relation['followers'] = self.user.followed_by
        relation['following'] = self.user.follows
        self.save('relation', json.dumps(relation))

        types = dict()
        types['images'] = media.filter(type='image').count()
        types['videos'] = media.filter(type='video').count()
        types['total'] = media.count()
        self.save('type', json.dumps(types))

    def _cache_analytics(self, media, *args, **kwargs):

        most = dict()
        # c = media.order_by('-comment_count')[:1]
        most['commented'] = self.serialize_obj(media.order_by('-comment_count')[:1])
        most['liked'] = self.serialize_obj(media.order_by('-like_count')[:1])
        self.save('most', json.dumps(most))
        # self.save('most', self.serialize_obj(most))

        # Will be saved in dashboard_cache
        # context['table'] = media.order_by('-like_count', '-comment_count')

        # WEEKDAYS SORT ON BIDI AND NORMAL LANGUGAES DIFFERENCE:
        # context['wk_fa'] = {0: 'Saturday', 1: 'Sunday', 2: 'Monday', 3: 'Tuesday', 4: 'Wednesday', 5: 'Thursday',
        #                     6: 'Friday'}
        # context['wk_en'] = {0: 'Sunday', 1: 'Monday', 2: 'Tuesday', 3: 'Wednesday', 4: 'Thursday', 5: 'Friday',
        #                     6: 'Saturday'}
        wkd = {0: 'Saturday', 1: 'Sunday', 2: 'Monday', 3: 'Tuesday', 4: 'Wednesday', 5: 'Thursday', 6: 'Friday'}
        wk_posts = [{'media': [m for m in media if m.created_time.weekday() == i], 'number': i, 'name': wkd[i]} for i in
                    range(7)]

        # # context['weekday'] = [[m for m in media if m.created_time.weekday() == i] for i in range(7)]
        # # Timezone change first and compute next
        # # from django.utils import timezone

        # GET CURRENT TIMEZONE FIRST, CHANGE POST TIMES IN CURRENT TIMEZONE AND CALCULATE THEM
        ctime = timezone.get_current_timezone()
        ptime = [{'range': '%s-%s' % (i * 4, (i + 1) * 4),
                  'media': [m for m in media if m.created_time.astimezone(ctime).hour in range(i * 4, (i + 1) * 4)]} for
                 i in range(6)]

        for i in wk_posts:
            i['like_count'] = CSum(i['media'], 'like_count')
            i['comment_count'] = CSum(i['media'], 'comment_count')
            try:
                i['like_avg'] = i['like_count'] / len(i['media'])
                i['comment_avg'] = i['comment_count'] / len(i['media'])
            except:
                i['like_avg'] = 0
                i['comment_avg'] = 0

        for i in ptime:
            i['like_count'] = CSum(i['media'], 'like_count')
            i['comment_count'] = CSum(i['media'], 'comment_count')

            try:
                i['like_avg'] = i['like_count'] / len(i['media'])
                i['comment_avg'] = i['comment_count'] / len(i['media'])
            except:
                i['like_avg'] = 0
                i['comment_avg'] = 0

        pt_notes = dict()
        pt_max_like = max([i['like_avg'] for i in ptime])
        pt_notes['max_like'] = {'ranges': [i['range'] for i in ptime if i['like_avg'] == pt_max_like],
                                'avg': pt_max_like}
        # self.save('ptime_like_avg_max', max([i['like_avg'] for i in ptime]))

        pt_min_like = min([i['like_avg'] for i in ptime])
        pt_notes['min_like'] = {'ranges': [i['range'] for i in ptime if i['like_avg'] == pt_min_like],
                                'avg': pt_min_like}
        # self.save('ptime_like_avg_min', min([i['like_avg'] for i in ptime]))

        pt_min_comment = min([i['comment_avg'] for i in ptime])
        pt_notes['min_comment'] = {'ranges': [i['range'] for i in ptime if i['comment_avg'] == pt_min_comment],
                                   'avg': pt_min_comment}
        # self.save('ptime_comment_avg_max', max([i['comment_avg'] for i in ptime]))

        pt_max_comment = max([i['comment_avg'] for i in ptime])
        pt_notes['max_comment'] = {'ranges': [i['range'] for i in ptime if i['comment_avg'] == pt_max_comment],
                                   'avg': pt_max_comment}
        # self.save('ptime_comment_avg_min', min([i['comment_avg'] for i in ptime]))

        pt_max_post = max([len(i['media']) for i in ptime])
        pt_notes['max_post'] = {'ranges': [i['range'] for i in ptime if len(i['media']) == pt_max_post],
                                'avg': pt_max_post}
        # self.save('ptime_max', max([len(i['media']) for i in ptime]))

        pt_min_post = min([len(i['media']) for i in ptime])
        pt_notes['min_post'] = {'ranges': [i['range'] for i in ptime if len(i['media']) == pt_min_post],
                                'avg': pt_min_post}
        # self.save('ptime_min', min([len(i['media']) for i in ptime]))

        self.save('pt_notes', json.dumps(pt_notes))

        wk_notes = dict()
        wk_max_like = max([i['like_avg'] for i in wk_posts])
        wk_notes['max_like'] = {'days': [i['name'] for i in wk_posts if i['like_avg'] == wk_max_like],
                                'avg': wk_max_like}
        # self.save('wk_posts_like_avg_max', max([i['like_avg'] for i in wk_posts]))

        wk_min_like = min([i['like_avg'] for i in wk_posts])
        wk_notes['min_like'] = {'days': [i['name'] for i in wk_posts if i['like_avg'] == wk_min_like],
                                'avg': wk_min_like}
        # self.save('wk_posts_like_avg_min', min([i['like_avg'] for i in wk_posts]))

        wk_min_comment = min([i['comment_avg'] for i in wk_posts])
        wk_notes['min_comment'] = {'days': [i['name'] for i in wk_posts if i['comment_avg'] == wk_min_comment],
                                   'avg': wk_min_comment}
        # self.save('wk_posts_comment_avg_min', min([i['comment_avg'] for i in wk_posts]))

        wk_max_comment = max([i['comment_avg'] for i in wk_posts])
        wk_notes['max_comment'] = {'days': [i['name'] for i in wk_posts if i['comment_avg'] == wk_max_comment],
                                   'avg': wk_max_comment}
        # self.save('wk_posts_comment_avg_max', max([i['comment_avg'] for i in wk_posts]))

        wk_max_post = max([len(i['media']) for i in wk_posts])
        wk_notes['max_post'] = {'days': [i['name'] for i in wk_posts if len(i['media']) == wk_max_post],
                                'avg': wk_max_post}
        # self.save('wk_posts_max', max([len(i['media']) for i in wk_posts]))

        wk_min_post = min([len(i['media']) for i in wk_posts])
        wk_notes['min_post'] = {'days': [i['name'] for i in wk_posts if len(i['media']) == wk_min_post],
                                'avg': wk_min_post}
        # self.save('wk_posts_min', min([len(i['media']) for i in wk_posts]))

        self.save('wk_notes', json.dumps(wk_notes))

        for t in ptime:
            t['media'] = self.serialize_obj(t['media'])
        self.save('ptime', json.dumps(ptime))

        for d in wk_posts:
            d['media'] = self.serialize_obj(d['media'])
        self.save('wk_posts', json.dumps(wk_posts))

    @classmethod
    def get_dashboard_cache(cls, site_user):
        user = cls(user=site_user)
        context = dict()

        context['tags'] = user.get('tags')
        context['tagged_users'] = user.get('tagged_users')
        context['day_streak'] = user.get('day_streak')

        chart = deserialize(user.get('chart'))
        limit = len(chart)
        if limit < 40:
            context['chart'] = chart
        else:
            context['chart'] = chart[limit-40:]

        table = user.get('table')
        table = deserialize(table)
        if len(table) < 5:
            context['table'] = table
        else:
            context['table'] = table[:5]

        context['ratio'] = json.loads(user.get('ratio'))

        context['like'] = json.loads(user.get('like'))
        context['comment'] = json.loads(user.get('comment'))

        context['relation'] = json.loads(user.get('relation'))
        context['type'] = json.loads(user.get('type'))

        return context

    @classmethod
    def get_analytic_cache(cls, site_user):
        user = cls(user=site_user)
        context = dict()

        table = user.get('table')
        context['table'] = deserialize(table)

        most = json.loads(user.get('most'))
        most['commented'] = deserialize(most['commented'])[0]
        most['liked'] = deserialize(most['liked'])[0]
        context['most'] = most

        context['pt_notes'] = json.loads(user.get('pt_notes'))

        ptime = json.loads(user.get('ptime'))
        for t in ptime:
            t['media'] = deserialize(t['media'])
        context['ptime'] = ptime

        context['wk_notes'] = json.loads(user.get('wk_notes'))

        wk_posts = json.loads(user.get('wk_posts'))
        for d in wk_posts:
            d['media'] = deserialize(d['media'])
        context['wk_posts'] = wk_posts

        context['chart'] = deserialize(user.get('chart'))
        return context

    @classmethod
    def delete(cls, site_user, *args, **kwargs):
        user = cls(site_user)
        user.connection.delete(user.user.id)

    def save(self, key, value):
        self.connection.hset(self.user.id, key, value)

    def get(self, key):
        return self.connection.hget(self.user.id, key)

    def serialize_obj(self, obj):
        temp = serializers.serialize('python', obj)
        return json.dumps(temp, cls=DjangoJSONEncoder)
