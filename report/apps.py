from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ReportConfig(AppConfig):
    name = 'report'
    verbose_name = _("Report")
    verbose_name_plural = _("Reports")
