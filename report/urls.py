from django.conf.urls import url
from django.core.urlresolvers import reverse

from . import views


urlpatterns = [
    url(r'create_comment_report/(?P<pid>.*)/$', views.create_comment_report, name='create_comment_report'),
    # url(r'print/$', views.printpdf, name='print_report'),
    # url(r'show/$', views.showpdf, name='show_report'),
]