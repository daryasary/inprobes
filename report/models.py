from __future__ import unicode_literals

from django.db import models
from django.db.models import Count
from django.utils.translation import ugettext_lazy as _

from index.models import UserProfile
from services.InstagramData.models import Tag, Media


class ReportBaseModel(models.Model):
    user = models.ForeignKey(
        UserProfile, verbose_name=_('User'), related_name='reports'
    )
    tag = models.ForeignKey(
        Tag, verbose_name=_('Hashtag'), related_name='reports'
    )
    active = models.BooleanField(default=True, verbose_name=_('Active'))

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('Report Base Model')
        verbose_name_plural = _('Report Base Models')

    def __unicode__(self):
        return '{} > {}'.format(self.user, self.tag.name)


class CommentReport(models.Model):
    user = models.ForeignKey(
        UserProfile, verbose_name=_('User'), related_name='comment_reports'
    )
    post = models.ForeignKey(
        Media, verbose_name=_('Post'), related_name='comment_reports'
    )
    expire_date = models.DateTimeField(
        verbose_name=_('Expire date'), null=True, blank=True
    )
    active = models.BooleanField(verbose_name=_('Active'), default=True)
    busy = models.BooleanField(verbose_name=_('Busy'), default=False)

    created_at = models.DateTimeField(
        verbose_name=_('Created'), auto_now_add=True
    )
    modified_at = models.DateTimeField(
        verbose_name=_('Modified'), auto_now=True
    )

    class Meta:
        ordering = ['-created_at']
        verbose_name = _('Comment Report Model')
        verbose_name_plural = _('Comment Report Model')

    def __unicode__(self):
        return '{} > {}'.format(self.user, self.post.id)

    def comments_count(self):
        return self.post.comments.count()
    comments_count.short_description = _("Comments count")

    def set_busy(self):
        self.busy = True
        self.save()

    def set_free(self):
        self.busy = False
        self.save()


class HashtagReport(models.Model):
    user = models.ForeignKey(
        UserProfile, verbose_name=_('User'), related_name='hashtag_reports'
    )
    tag = models.ForeignKey(
        Tag, verbose_name=_('Hashtag'), related_name='hashtag_reports'
    )
    expire_date = models.DateTimeField(
        verbose_name=_('Expire date'), null=True, blank=True
    )
    active = models.BooleanField(default=True, verbose_name=_('Active'))
    busy = models.BooleanField(verbose_name=_('Busy'), default=False)

    created = models.DateTimeField(
        verbose_name=_('Created'), auto_now_add=True
    )
    modified = models.DateTimeField(
        verbose_name=_('Modified'), auto_now=True
    )

    class Meta:
        verbose_name = _('HashtagReport')
        verbose_name_plural = _('HashtagReports')

    def __unicode__(self):
        return '{} > {}'.format(self.user, self.tag.name)

    @property
    def hashtag(self):
        return "#" + self.tag.name

    @property
    def participants(self):
        return self.tag.media.values('user').annotate(dd=Count('user')) or []

    @property
    def relevant_tags(self):
        tid = self.tag.media.values_list('id')
        return Tag.objects.filter(media__id__in=tid)

    def media_count(self):
        return self.tag.media_count
    media_count.short_description = _("Media count")

    def set_busy(self):
        self.busy = True
        self.save()

    def set_free(self):
        self.busy = False
        self.save()
