from __future__ import absolute_import

from celery.task.schedules import crontab
from celery.decorators import task, periodic_task
from celery.utils.log import get_task_logger

from report.models import CommentReport, HashtagReport

from services.InstagramData.utils import CommentHandler, HashtagHandler

logger = get_task_logger(__name__)


# TODO: Task disabled, may remove later
# @periodic_task(run_every=(crontab(minute=0, hour='8,10,14,16,17,19,20,23')),
#                name="Comment manager", ignore_result=True)
def comment_manager():
    """Update All users Accounts periodically"""
    comment_reports = CommentReport.objects.filter(
        active=True, busy=False
    ).values_list('id', flat=True)
    for report in comment_reports:
        comment_handler.delay(report)
    return "%s report handler called" % comment_reports.count()


# TODO: Task disabled, may remove later
# @task(name='Comment handler')
def comment_handler(report_id):
    try:
        report = CommentReport.objects.get(id=report_id, busy=False)
    except CommentReport.DoesNotExist:
        return "Comment report handler called with invalid report #%s" % report_id
    else:
        report.set_busy()
        comments, result = CommentHandler.get_comments(
            report.user.instagram_id, report.post
        )
        report.set_free()

        if not result:
            report.user.userprofile.set_instageram_id_to_null()
            logger.warning(
                "Profile %s encountered Instagram API Error" % report.user
            )
            return "User profile access token error"

    return "Comment report handler done for report #%s(%s cm)" % (
        report_id, len(comments))


# TODO: Task disabled, may remove later
# @periodic_task(
#     run_every=(crontab(minute=0, hour='8,10,14,16,17,19,20,23')),
#     name="Hashtag manager", ignore_result=True
# )
def hashtag_manager():
    hashtag_reports = HashtagReport.objects.filter(
        active=True, busy=False
    ).values_list('id', flat=True)
    for report in hashtag_reports:
        hashtag_handler.delay(report)
    return "%s report handler called" % hashtag_reports.count()


# TODO: Task disabled, may remove later
# @task(name='Hashtag handler')
def hashtag_handler(report_id):
    try:
        report = HashtagReport.objects.get(id=report_id, busy=False)
    except HashtagReport.DoesNotExist:
        return "Hashtag report handler called with invalid report #%s" % report_id
    else:
        report.set_busy()
        try:
            hashtag, media = HashtagHandler.update_media(
                report.user.instagram_id, report.tag
            )
        except Exception as e:
            report.set_free()
            return "Hashtag report handler encountered error %s" % e
        else:
            report.set_free()
            if media is None:
                report.user.userprofile.set_instageram_id_to_null()
                logger.warning(
                    "Profile %s encountered Instagram API Error" % report.user
                )
                return "User profile access token error"

    return "Hashtag report handler done for report #%s(%s media)" % (
        report_id, len(media))
