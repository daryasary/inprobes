from django.contrib.auth.decorators import login_required
from django.core.files.storage import FileSystemStorage
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.urlresolvers import reverse
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader
from django.conf import settings

from weasyprint import HTML, CSS

from weasyprint.fonts import FontConfiguration
import pygal
from pygal.style import Style

from index.models import UserProfile
from report.models import CommentReport
from report.tasks import comment_handler
from services.InstagramData.models import Media
from statistics.utils.compute import RedisCache


def showtablepdf(request):
    user = request.user
    profile = UserProfile.load(user.id)
    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = RedisCache.get_analytic_cache(user)
    paginator = Paginator(context['table'], 20)

    page = request.GET.get('page')
    try:
        context['table'] = paginator.page(page)
    except PageNotAnInteger:
        context['table'] = paginator.page(1)
    except EmptyPage:
        context['table'] = paginator.page(paginator.num_pages)

    bar_chart = pygal.Bar()
    bar_chart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
    bar = bar_chart.render(is_unicode=True)

    context['nav_bar_side'] = 'analytic'
    return render(request, 'report/pdf_report.html',
              {'context': context, 'profile': profile, 'user': user, 'bar': bar})


def showpdf(request):
    bar_chart = pygal.Bar()
    bar_chart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
    # bar = bar_chart.render(is_unicode=True)
    # bar = bar_chart.render_data_uri()

    bar_root = settings.MEDIA_ROOT + '/tmp/chart4.png'
    bar_chart.render_to_png(bar_root)

    bar_url = settings.MEDIA_URL + 'tmp/chart4.png'

    return render(request, 'report/chart_report.html', {'bar': bar_url})


def printpdf(request):
    custom_style = Style(colors=('#E853A0'))
    # bar_chart = pygal.Bar(style=custom_style)
    bar_chart = pygal.Bar()
    bar_chart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])

    bar_root = settings.MEDIA_ROOT + '/tmp/chart6.png'

    bar_chart.render_to_png(bar_root)

    bar_url = settings.MEDIA_URL + 'tmp/chart6.png'

    template = loader.get_template('report/chart_report.html')

    context = {'bar': bar_root}
    
    html_string = template.render(context)

    font_config = FontConfiguration()
    custom = settings.STATIC_ROOT + "/report/css/style.css"
    css = [CSS(custom)]

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf', stylesheets = css, font_config=font_config)

    fs = FileSystemStorage('/tmp')

    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="mypdf.pdf"'
        return response


def printtablespdf(request):
    user = request.user
    profile = UserProfile.load(user.id)
    if not profile.has_instagram or not profile.parsed or not profile.confirm:
        return redirect('account.views.complete_registration')

    context = RedisCache.get_analytic_cache(user)
    paginator = Paginator(context['table'], 20)

    page = request.GET.get('page')
    try:
        context['table'] = paginator.page(page)
    except PageNotAnInteger:
        context['table'] = paginator.page(1)
    except EmptyPage:
        context['table'] = paginator.page(paginator.num_pages)

    context['nav_bar_side'] = 'analytic'


    bar_chart = pygal.Bar()
    bar_chart.add('Fibonacci', [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
    bar = bar_chart.render(is_unicode=True)
    # bar = bar_chart.render_tree()

    # print bar

    template = loader.get_template('index/report/pdf_report.html')

    context = {'context': context, 'profile': profile, 'user': user, 'bar': bar}
    
    html_string = template.render(context)

    font_config = FontConfiguration()
    bootstrap = settings.STATIC_ROOT + "/index/assets/css/bootstrap.css"
    custom = settings.STATIC_ROOT + "/index/assets/css/pdf_style.css"
    fontawesome = settings.STATIC_ROOT + "/index/assets/css/font-awesome.css"
    morris = settings.STATIC_ROOT + "/index/assets/js/morris/morris-0.4.3.min.css"
    css = [CSS(bootstrap), CSS(custom), CSS(fontawesome), CSS(morris)]

    html = HTML(string=html_string)
    html.write_pdf(target='/tmp/mypdf.pdf', stylesheets = css, font_config=font_config)

    fs = FileSystemStorage('/tmp')

    with fs.open('mypdf.pdf') as pdf:
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="mypdf.pdf"'
        return response

################################################################################
########################### OLD PDF TABLES #####################################
################################################################################


@login_required
def create_comment_report(request, pid):
    profile = request.user.userprofile
    insta_user = profile.instagram_id
    dic = {'id': pid, 'user': insta_user}
    post = get_object_or_404(Media, **dic)
    report, result = CommentReport.objects.get_or_create(
        user=profile, post=post
    )
    if not report.active:
        report.active = True
        report.save()
    # comment_handler.delay(report.id)
    return redirect(reverse('post_detail', args=[pid]))
