from django.contrib import admin
from report.models import ReportBaseModel, CommentReport, HashtagReport


class ReportBaseModelAdmin(admin.ModelAdmin):
    list_display = ['tag', 'active', 'modified', 'created']


class CommentReportAdmin(admin.ModelAdmin):
    list_display = ['user', 'post', 'active', 'busy', 'comments_count', 'created_at']


class HashtagReportAdmin(admin.ModelAdmin):
    list_display = ['user', 'tag', 'active', 'busy', 'media_count', 'expire_date', 'created']


admin.site.register(ReportBaseModel, ReportBaseModelAdmin)
admin.site.register(CommentReport, CommentReportAdmin)
admin.site.register(HashtagReport, HashtagReportAdmin)
