import pygal
import pytz
from datetime import datetime

from django.core.management.base import BaseCommand
from django.utils import timezone
from django.db import connection
from django.db.models import Sum, Count, Avg

from services.InstagramData.models import Media, Tag, User, Location


class Command(BaseCommand):

    def handle(self, *args, **options):
        path = '/home/shiva/PycharmProjects/inporbes/inprobes/media/reports/tmp23'
        hashtag = Tag.objects.get(pk=4056)
        er_day = datetime.strptime('2017-11-12 03:30:00', '%Y-%m-%d %H:%M:%S')
        queryset = hashtag.media.filter(created_time__gte=er_day)

        truncate_date = connection.ops.date_trunc_sql('month', 'created_time')
        qs = queryset.extra({'month': truncate_date})

        # timezone.localtime(pytz.utc.localize(i['hour']),
        #                    pytz.timezone('Asia/Tehran')).hour, i['pk__count']
        # x = ['Nov-12 03:30', 'Nov-12 05:30', 'Nov-12 06:30', 'Nov-12 07:30', 'Nov-12 08:30', 'Nov-12 09:30', 'Nov-12 10:30', 'Nov-12 11:30', 'Nov-12 12:30', 'Nov-12 13:30', 'Nov-12 14:30', 'Nov-12 15:30', 'Nov-12 16:30', 'Nov-12 17:30', 'Nov-12 18:30', 'Nov-12 19:30', 'Nov-12 20:30', 'Nov-12 21:30', 'Nov-12 22:30', 'Nov-12 23:30', 'Nov-13 00:30', 'Nov-13 01:30', 'Nov-13 02:30', 'Nov-13 03:30', 'Nov-13 04:30', 'Nov-13 05:30', 'Nov-13 06:30', 'Nov-13 07:30', 'Nov-13 08:30', 'Nov-13 09:30', 'Nov-13 10:30', 'Nov-13 11:30', 'Nov-13 12:30', 'Nov-13 13:30', 'Nov-13 14:30', 'Nov-13 15:30', 'Nov-13 16:30', 'Nov-13 17:30', 'Nov-13 18:30', 'Nov-13 19:30', 'Nov-13 20:30', 'Nov-13 21:30', 'Nov-13 22:30']
        # y = [1, 3, 3, 3, 4, 6, 5, 8, 6, 5, 7, 14, 9, 8, 23, 21, 27, 433, 1308, 1319, 942, 601, 393, 252, 154, 191, 412, 862, 1490, 2357, 2900, 3183, 3435, 3508, 3047, 2649, 2385, 2252, 2259, 2346, 2571, 2734, 2712]

        post_count = qs.values('month').annotate(Count('pk')).order_by('month')
        # post_count_line_chart = pygal.Line(x_label_rotation=55)
        #
        post_count_line_chart = pygal.StackedLine(fill=True, x_label_rotation=55, width=1000)
        post_count_line_chart.title = 'Posts density monthly'
        x = [datetime.strftime(timezone.localtime(pytz.utc.localize(i['month']),pytz.timezone('Asia/Tehran')), '%b-%d %H:%M') for i in post_count]
        y = [i['pk__count'] for i in post_count]

        post_count_line_chart.x_labels = map(str, x)
        post_count_line_chart.add('Post count', y)
        post_count_line_chart_path = path + '/post_count_time_line.png'
        post_count_line_chart.render_to_png(post_count_line_chart_path)

        like_count = qs.values('month').annotate(avg=(Avg('like_count'))).order_by('month')
        like_count_line_chart = pygal.StackedLine(fill=True, x_label_rotation=55, width=1000)
        like_count_line_chart.title = "Post traction"
        like_count_line_chart.x_labels = map(str, x)
        y = [i['avg'] for i in like_count]
        like_count_line_chart.add('Likes avg', y)
        p = path + '/like_count_time_line.png'
        like_count_line_chart.render_to_png(p)

        comment_count = qs.values('month').annotate(avg=Avg('comment_count')).order_by('month')
        comment_count_line_chart = pygal.StackedLine(fill=True, x_label_rotation=55, width=1000)
        comment_count_line_chart.title = "Post traction"
        comment_count_line_chart.x_labels = map(str, x)
        y = [i['avg'] for i in comment_count]
        comment_count_line_chart.add('Comments avg', y)
        p = path + '/comment_count_time_line.png'
        comment_count_line_chart.render_to_png(p)

        mid = qs.values_list('id', flat=True)

        tags_qs = Tag.objects.filter(media__id__in=mid).values('name').annotate(count_media=Count('media')).order_by('-count_media').exclude(name=hashtag.name)[:20]
        related_hashtags = pygal.HorizontalBar(width=1000)
        related_hashtags.title = "Tag title"
        related_hashtags.x_labels = [tag['name'] for tag in tags_qs]
        related_hashtags.add('Related hashtags', [tag['count_media'] for tag in tags_qs])
        related_hashtags_path = path + '/related_hashtags.png'
        related_hashtags.render_to_png(related_hashtags_path)

        users_qs = User.objects.filter(posted_media__id__in=mid).values('username').annotate(media_count=Count('posted_media')).order_by('-media_count')[:20]
        active_accounts = pygal.HorizontalBar(width=1000)
        active_accounts.title = "Users Tile"
        active_accounts.x_labels = [tag['username'] for tag in users_qs]
        active_accounts.add('Users density', [tag['media_count'] for tag in users_qs])
        active_accounts_path = path + '/active_accounts.png'
        active_accounts.render_to_png(active_accounts_path)

        location_qs = Location.objects.filter(media_location__id__in=mid).values('name').annotate(media_count=Count('media_location')).order_by('-media_count')[:20]
        posts_locations = pygal.HorizontalBar(width=1000)
        posts_locations.title = "Locations title"
        posts_locations.x_labels = [tag['name'] for tag in location_qs]
        posts_locations.add('Post locations', [tag['media_count'] for tag in location_qs])
        posts_locations_path = path + '/posts_locations.png'
        posts_locations.render_to_png(posts_locations_path)
