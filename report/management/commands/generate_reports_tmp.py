import os

import logging
import pygal
import pytz
import collections
from django.template import loader
from datetime import datetime

from pygal.style import Style

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from django.conf import settings
from django.db import connection
from django.db.models import Sum, Count, Avg, Q

from weasyprint import HTML, CSS
from weasyprint.fonts import FontConfiguration

from services.InstagramData.models import Media, Tag, User, Location
from services.InstagramData.utils import HashtagHandler
from report.models import ReportBaseModel


class Command(BaseCommand):

    # TODO: https://stackoverflow.com/questions/18216198/unable-to-load-timezones

    def init_path(self, target):
        folder = target.user.user.username + '_' + str(target.id)
        parent = settings.REPORTS_ROOT + folder
        if not os.path.exists(parent):
            os.makedirs(parent)

        self.path = parent + '/' + timezone.now().strftime("%y-%b-%d %I:%M") + '/tmp'
        if not os.path.exists(self.path):
            os.makedirs(self.path)

    def init_logger(self, target):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)

        # create a file handler
        path = self.path + '/' + target.tag.name + '.log'
        handler = logging.FileHandler(path)
        handler.setLevel(logging.INFO)

        # create a logging format
        formatter = logging.Formatter(
            '%(asctime)s - [%(levelname)s] - %(message)s')
        handler.setFormatter(formatter)

        # add the handlers to the logger
        self.logger.addHandler(handler)

        self.logger.info('Logger initiated...')

    def generate_time_change_bar(self, queryset):
        time_ordered_qs = queryset.order_by('created_time')

        bar_chart = pygal.StackedBar(x_label_rotation=50, width=1000)
        bar_chart.title = "Hashtag timeline"
        # custom_style = Style(colors=('#E853A0'))
        # bar_chart = pygal.Bar(style=custom_style)

        # group by month
        truncate_date = connection.ops.date_trunc_sql('month', 'created_time')
        qs = queryset.extra({'month': truncate_date})
        total_image = qs.values('month').filter(type='image').annotate(image=Count('pk')).order_by('month')
        total_video = qs.values('month').filter(type='video').annotate(video=Count('pk')).order_by('month')
        bar_chart.x_labels = [i['month'].strftime('%y-%b') for i in total_image]

        bar_chart.add('Image', [i['image'] for i in total_image])
        bar_chart.add('Video', [i['video'] for i in total_video])

        time_change_post_path = self.path + '/time_change_post.png'

        bar_chart.render_to_png(time_change_post_path)
        return time_change_post_path

    def time_interaction_post_bar(self, queryset):
        interaction_bar_chart = pygal.StackedBar(x_label_rotation=50,
                                                 width=1000)
        interaction_bar_chart.title = "Interaction timeline"
        custom_style = Style(colors=('#E853A0'))
        # # bar_chart = pygal.Bar(style=custom_style)

        # group by month
        truncate_date = connection.ops.date_trunc_sql('month', 'created_time')
        qs = queryset.extra({'month': truncate_date})
        total_image = qs.values('month').filter(type='image').annotate(
            image=Count('pk')
        ).order_by('month')

        total_likes = qs.values('month').annotate(
            like=Sum('like_count')
        ).order_by('month')

        total_comments = qs.values('month').annotate(
            comment=Sum('comment_count')
        ).order_by('month')

        interaction_bar_chart.x_labels = [i['month'].strftime('%y-%b') for i in total_image]

        interaction_bar_chart.add('Likes', [i['like'] for i in total_likes])
        interaction_bar_chart.add('Comments', [i['comment'] for i in total_comments])

        time_interaction_post_path = self.path + '/time_interaction_post.png'
        interaction_bar_chart.render_to_png(time_interaction_post_path)
        return time_interaction_post_path

    def time_interaction_post_line(self, queryset):
        interaction_rate_chart = pygal.Line(x_label_rotation=50, width=1000)
        interaction_rate_chart.title = "Interaction(avg) timeline"
        # custom_style = Style(colors=('#E853A0'))
        # # bar_chart = pygal.Bar(style=custom_style)

        # group by month
        truncate_date = connection.ops.date_trunc_sql('month', 'created_time')
        qs = queryset.extra({'month': truncate_date})
        total_posts = qs.values('month').annotate(
            post=Count('pk')
        ).order_by('month')
        total_image = qs.values('month').filter(type='image').annotate(
            image=Count('pk')
        ).order_by('month')

        total_likes = qs.values('month').annotate(
            like=Sum('like_count')
        ).order_by('month')

        total_comments = qs.values('month').annotate(
            comment=Sum('comment_count')
        ).order_by('month')

        interaction_rate_chart.x_labels = [i['month'].strftime('%y-%b') for i in total_image]

        interaction_rate_chart.add('Interaction rate', [(total_likes[i]['like'] + total_comments[i]['comment']) / total_posts[i]['post'] for i in range(len(total_posts))])
        interaction_rate_chart_path = self.path + '/interaction_rate_chart.png'
        interaction_rate_chart.render_to_png(interaction_rate_chart_path)
        return interaction_rate_chart_path

    def internal_hashtag_rate_bar_horizontal(self, queryset, hashtag):
        internal_hashatag_rate = pygal.HorizontalBar(width=1000)
        internal_hashatag_rate.title = "Tag title"
        custom_style = Style(colors=('#E853A0'))

        tags_queryset = Tag.objects.filter(
            media__id__in=queryset.values_list('pk', flat=True)).annotate(
            count_media=Count('media')).order_by('-count_media').exclude(
            name=hashtag.name)[:30]

        internal_hashatag_rate.x_labels = [tag.name for tag in tags_queryset]
        internal_hashatag_rate.add('Other hashtags',
                                   [tag.count_media for tag in tags_queryset])
        internal_hashtag_rate_path = self.path + '/internal_hashatag_rate.png'
        internal_hashatag_rate.render_to_png(internal_hashtag_rate_path)
        return internal_hashtag_rate_path

    def post_count_line_chart(self, queryset):
        post_count = queryset.values('month').annotate(Count('pk')).order_by('month')

        post_count_line_chart = pygal.StackedLine(
            fill=True, x_label_rotation=55, width=1000
        )
        post_count_line_chart.title = 'Posts density monthly'
        x = [datetime.strftime(
            timezone.localtime(
                pytz.utc.localize(i['month']), pytz.timezone('Asia/Tehran')
            ), '%y-%b'
        ) for i in post_count]

        y = [i['pk__count'] for i in post_count]
        post_count_line_chart.x_labels = map(str, x)
        post_count_line_chart.add('Post count', y)
        post_count_line_chart_path = self.path + '/post_count_time_line.png'
        post_count_line_chart.render_to_png(post_count_line_chart_path)
        return post_count_line_chart_path

    def like_count_line_chart(self, queryset):
        like_count = queryset.values('month').annotate(
            avg=(Avg('like_count'))).order_by('month')
        like_count_line_chart = pygal.StackedLine(
            fill=True,  x_label_rotation=55, width=1000
        )

        x = [datetime.strftime(
            timezone.localtime(
                pytz.utc.localize(i['month']), pytz.timezone('Asia/Tehran')
            ), '%y-%b'
        ) for i in like_count]

        like_count_line_chart.title = "Post traction"
        like_count_line_chart.x_labels = map(str, x)
        y = [i['avg'] for i in like_count]
        like_count_line_chart.add('Likes avg', y)
        p = self.path + '/like_count_time_line.png'
        like_count_line_chart.render_to_png(p)
        return p

    def comment_count_line_chart(self, queryset):
        comment_count = queryset.values('month').annotate(
            avg=Avg('comment_count')
        ).order_by('month')
        comment_count_line_chart = pygal.StackedLine(fill=True, width=1200)
        comment_count_line_chart.title = "Post traction"
        a = {}
        for i in comment_count:
            if i['month'].year in a.keys():
                a[i['month'].year].append(i['avg'])
            else:
                a[i['month'].year] = []

        comment_count_line_chart.x_labels = ['November', 'December', 'January', 'Feburary', 'March']

        # y = [i['avg'] for i in comment_count]
        # comment_count_line_chart.add('Comments avg', y)
        od = collections.OrderedDict(sorted(a.items()))
        for i in od:
            comment_count_line_chart.add('{} Comments avg'.format(i), od[i])
        p = self.path + '/comment_count_time_line.png'
        comment_count_line_chart.render_to_png(p)
        return p

    def active_accounts_bar_chart(self, queryset):
        mid = queryset.values_list('id', flat=True)
        users_qs = User.objects.filter(posted_media__id__in=mid).values(
            'username').annotate(media_count=Count('posted_media')).order_by(
            '-media_count')[:20]
        active_accounts = pygal.HorizontalBar(width=1000)
        active_accounts.title = "Most active users"
        active_accounts.x_labels = [tag['username'] for tag in users_qs]
        active_accounts.add('Users density', [tag['media_count'] for tag in users_qs])
        active_accounts_path = self.path + '/active_accounts.png'
        active_accounts.render_to_png(active_accounts_path)
        return active_accounts_path

    def posts_locations_bar_chart(self, queryset):
        mid = queryset.values_list('id', flat=True)
        location_qs = Location.objects.filter(
            media_location__id__in=mid
        ).values('name').annotate(
            media_count=Count('media_location')
        ).order_by('-media_count')[:20]
        posts_locations = pygal.HorizontalBar(width=1000)
        posts_locations.title = "Locations title"
        posts_locations.x_labels = [tag['name'] for tag in location_qs]
        posts_locations.add('Post locations',
                            [tag['media_count'] for tag in location_qs])
        posts_locations_path = self.path + '/posts_locations.png'
        posts_locations.render_to_png(posts_locations_path)
        return posts_locations_path

    def handle(self, *args, **options):
        handler = HashtagHandler

        # Fetch all targets from db
        targets = ReportBaseModel.objects.filter(active=True)

        # Iterate over target one by one
        for target in targets:
            hashtag = target.tag
            user = target.user
            context = dict()

            self.init_path(target)
            self.init_logger(target)

            self.logger.info('Start fetching media for #{}'.format(hashtag))
            # Get all posts for given hashtag
            # create new class inherit from InstagramHandler called TagHandler
            # get a hashtag and maybe start/end time, fetch all posts for it,
            # save them on media table and return a list of media instances
            # hashtag_result, media = handler.update_media(user.instagram_id, hashtag)
            # self.logger.info('{} Media fetched for #{}'.format(len(media), hashtag))

            # self.logger.info('Additional data for each media in #{}'.format(hashtag))
            # Start get complete details about all medias from list above
            # Maybe should be done by thread !
            # Store them and return a list of Media instances again
            # self.logger.info('Additional data for each media in #{} ended up'.format(hashtag))

            self.logger.info('Fetching data from instagram ended, data mining is starting')

            # queryset = hashtag.media.all()
            # queryset = hashtag.media.filter(Q(created_time__month__gte=11) | Q(created_time__month__lte=3))
            queryset = hashtag.media.filter(Q(created_time__month__gte=11) | Q(created_time__month__lte=3))
            # # If period of time has no sense, else:
            # # queryset = Media.objects.filter(tag=hashtag, created_at__gte=time, created_at__lte=time)
            #
            # ##################
            # ### CHART NO 1 ###
            # ##################

            context['time_change_post_path'] = self.generate_time_change_bar(queryset)

            # ##################
            # ### CHART NO 2 ###
            # ##################

            context['time_change_post_bar_path'] = self.time_interaction_post_bar(queryset)

            # ##################
            # ### CHART NO 3 ###
            # ##################
            context['time_change_post_line_path'] = self.time_interaction_post_line(queryset)
            # ##################
            # ### CHART NO 4 ###
            # ##################
            context['internal_hashtag_rate_bar_horizontal'] = self.internal_hashtag_rate_bar_horizontal(queryset, hashtag)

            # ##################
            # ### CHART NO 5 ###
            # ##################
            truncate_date = connection.ops.date_trunc_sql('month', 'created_time')
            qs = queryset.extra({'month': truncate_date})

            context['post_count_line_chart_path'] = self.post_count_line_chart(qs)

            context['like_count_line_chart_path'] = self.like_count_line_chart(qs)

            context['comment_count_line_chart_path'] = self.comment_count_line_chart(qs)

            context['active_accounts_chart_path'] = self.active_accounts_bar_chart(qs)

            context['active_accounts_chart_path'] = self.active_accounts_bar_chart(qs)

            context['posts_locations_chart_path'] = self.posts_locations_bar_chart(qs)

            # #################################
            # # Create PDF template
            # template = loader.get_template('report/chart_report.html')
            #
            # html_string = template.render(context)
            #
            # font_config = FontConfiguration()
            # custom = settings.STATIC_ROOT + "/report/css/style.css"
            # font_awesome = settings.STATIC_ROOT + "/report/font-awesome-4.7.0/css/font-awesome.min.css"
            # css = [CSS(custom), CSS(font_awesome)]
            #
            # report_path = self.path + '/report.pdf'
            # html = HTML(string=html_string)
            # html.write_pdf(target=report_path, stylesheets=css, font_config=font_config)
