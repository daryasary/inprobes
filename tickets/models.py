from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from django.utils.html import format_html
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

from ckeditor.fields import RichTextField


class Response(models.Model):
    user = models.ForeignKey(User)
    ticket = models.ForeignKey('Ticket')
    text = RichTextField()
    date = models.DateTimeField(default=timezone.now)

    class Meta:
        ordering = ['date']
        verbose_name = _('Response')
        verbose_name_plural = _('Responses')

    def __unicode__(self):
        return self.text

    def save(self, *args, **kwargs):
        self.ticket.modified_time = timezone.now()
        if self.user != self.ticket.user:
            self.ticket.status = 'N'
        self.ticket.save()
        super(Response, self).save(*args, **kwargs)


class Ticket(models.Model):
    RELEVANT_CHOICES = (
        ('F', _('Financial')),
        ('T', _('Technical')),
        ('O', _('Others')),
    )

    PERIORITY_CHOICES = (
        ('H', _('High')),
        ('M', _('Middle')),
        ('L', _('Low')),
    )

    STATUS_CHOICES = (
        ('C', _('Closed')),
        ('A', _('Asked')),
        ('N', _('Answered')),
    )

    user = models.ForeignKey(User)
    subject = models.CharField(max_length=255, default=_('Type your subject here ...'))
    message = models.TextField()
    periority = models.CharField(max_length=1, choices=PERIORITY_CHOICES, default='M')
    relevant = models.CharField(max_length=1, choices=RELEVANT_CHOICES, default='T')
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default='A')
    score = models.PositiveSmallIntegerField(default=3)
    created_time = models.DateTimeField(editable=False, default=timezone.now)
    modified_time = models.DateTimeField(editable=False, default=timezone.now)

    class Meta:
        ordering = ['-modified_time']
        verbose_name = _('Ticket')
        verbose_name_plural = _('Tickets')

    def __unicode__(self):
        return self.subject

    def save(self, *args, **kwargs):
        self.modified_time = timezone.now()
        super(Ticket, self).save(*args, **kwargs)

    def show_status(self):
        color = {'C': 'Blue', 'A': 'red', 'N': 'green'}
        for S in self.STATUS_CHOICES:
            if self.status == S[0]:
                state = S[1]
                return format_html('<span style="color: {};">{}</span>', color[self.status], S[1])
            # return S[1]

    show_status.short_description = 'Status'

    def show_periority(self):
        for P in self.PERIORITY_CHOICES:
            if self.periority == P[0]:
                return P[1]

    show_periority.short_description = 'Periority'

    def show_relevant(self):
        for R in self.RELEVANT_CHOICES:
            if self.relevant == R[0]:
                return R[1]

    show_relevant.short_description = 'Relevant'

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse('ticket', args=[self.id])
