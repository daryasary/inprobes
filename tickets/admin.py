from django.contrib import admin
from .models import Response, Ticket


# # Register your models here.
# class ResponseAdmin(admin.ModelAdmin):
#     list_display = ['user', 'ticket', 'text', 'date']
#     exclude = ['user']
#
#     def save_model(self, request, obj, form, change):
#         obj.user = request.user
#         obj.save()


class ResponseInline(admin.StackedInline):
    model = Response
    # exclude = ['user']
    extra = 1


class TicketAdmin(admin.ModelAdmin):
    search_fields = ('user',)
    inlines = [ResponseInline, ]
    list_filter = ('status', 'relevant', 'periority')
    list_display = ['user', 'subject', 'message', 'show_relevant',
                    'show_status', 'show_periority', 'created_time',
                    'modified_time']

    readonly_fields = ('user', 'periority', 'relevant', 'score')
    fieldsets = (
        (None, {
            'fields': (
                ('subject', 'status',),
                'message',
                ('user', 'periority', 'relevant', 'score',),
            )
        }),)

    def save_formset(self, request, form, formset, change):
        instances = formset.save(commit=False)
        for instance in instances:
            instance.user = request.user
            instance.save()
        formset.save_m2m()

    def save_model(self, request, obj, form, change):
        obj.status = 'N'
        obj.save()


admin.site.register(Ticket, TicketAdmin)
# admin.site.register(Response, ResponseAdmin)
