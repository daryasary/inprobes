from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Ticket, Response

from simplemathcaptcha.fields import MathCaptchaField
from simplemathcaptcha.widgets import MathCaptchaWidget


class TicketForm(forms.ModelForm):
    captcha = MathCaptchaField(widget=MathCaptchaWidget(
        question_tmpl=_('what is %(num1)i %(operator)s %(num2)i? wirte now')))

    class Meta:
        model = Ticket
        fields = ('relevant', 'periority', 'subject', 'message',)

        widgets = {
            'relevant': forms.Select(attrs={'class': 'form-control'}),
            'periority': forms.Select(attrs={'class': 'form-control'}),
            'subject': forms.TextInput(attrs={'class': 'form-control',
                                              'placeholder': "Describe yourself here...",
                                              'name': 'subject'}),
            'message': forms.Textarea(
                attrs={'class': 'form-control', 'rows': '3'})
        }


class ResponseForm(forms.ModelForm):
    captcha = MathCaptchaField(widget=MathCaptchaWidget(
        question_tmpl=_('what is %(num1)i %(operator)s %(num2)i?')))

    class Meta:
        model = Response
        fields = ('text', 'captcha')
        widgets = {
            'text': forms.Textarea(
                attrs={'class': 'form-control', 'rows': '3'}, ),
        }
