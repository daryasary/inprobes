from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class TicketsConfig(AppConfig):
    name = _('tickets')
    verbose_name = _("Ticket")
    verbose_name_plural = _("Tickets")
