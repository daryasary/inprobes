from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from index.models import UserProfile
from tickets.forms import ResponseForm, TicketForm
from tickets.models import Ticket


@login_required
def support(request, ticket=None):
    user = request.user
    profile = UserProfile.load(user.id)
    context = dict()
    context['nav_bar_side'] = 'support'

    # If ticket is not None we should find and show the
    #  ticket thread and responses
    if ticket is not None:
        try:
            tk = Ticket.objects.get(id=ticket)
            if request.method == 'POST':
                # Response submited
                rs = ResponseForm(request.POST)
                if rs.is_valid():
                    response = rs.save(commit=False)
                    response.user = request.user
                    response.ticket = tk
                    response.save()
                    tk.status = "A"
                    tk.save()
                    context['result'] = True
                else:
                    context['error'] = True
            if tk.user == request.user:
                context['ticket'] = tk
                context['responses'] = tk.response_set.all()
                context['response_form'] = ResponseForm()
                # print context['ticket'], context['responses']
                return render(request, 'dashboard/tickets.html',
                              {'context': context, 'profile': profile,
                               'user': user})
        except:
            pass

    # Fetch all threads for main support page
    context['tickets'] = Ticket.objects.filter(user=request.user)

    if request.method == "POST":
        tk = TicketForm(request.POST)
        if tk.is_valid():
            ticket = tk.save(commit=False)
            ticket.user = request.user
            ticket.save()
            context['result'] = True
        else:
            context['error'] = True

    context['form'] = TicketForm()
    return render(request, 'dashboard/support.html',
                  {'context': context, 'profile': profile, 'user': user, 'nav_bar_side': 'support'})


