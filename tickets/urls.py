from django.conf.urls import url

from tickets.views import support

urlpatterns = [
    url(r'^$', support, name='support'),
    url(r'^ticket/([0-9]+)/$', support, name='ticket'),
]
