from django.conf.urls import url
from dashboard.api import views


urlpatterns = [
    url(r'^set_facebook_account/$', views.set_facebook_account, name='set_facebook_account'),
    url(r'^save_instagram_data/$', views.save_instagram_data, name='save_instagram_data'),
    url(r'^select_account/(?P<ig_ac_id>[0-9].*)/$', views.select_account, name='select_account'),
    url(r'^get_accounts/$', views.get_accounts, name='get_accounts'),
    url(r'^get_progress/$', views.get_progress, name='get_progress'),
]