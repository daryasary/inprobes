import json

from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from index.models import FacebookCredentials, InstagramAccounts
from services.graph_api.models import Account
from services.graph_api.tasks import graph_media_updater


@login_required
def set_facebook_account(request):
    data = json.loads(request.body)
    if data['status'] == 'connected':
        auth = data['authResponse']
        cred, result = FacebookCredentials.objects.get_or_create(
            expires_in=auth['expiresIn'], user_id=auth['userID'],
            data_access_expirations_time=auth['data_access_expiration_time'],
            access_token=auth['accessToken'], profile=request.user.userprofile,
            signed_request=auth['signedRequest'],
        )
    return JsonResponse(data)


@login_required
def save_instagram_data(request):
    data = json.loads(request.body)
    acid = data.pop('id')
    account, result = Account.objects.update_or_create(id=acid, defaults=data)
    account_profile, result = InstagramAccounts.objects.get_or_create(
        account=account, profile=request.user.userprofile
    )
    return JsonResponse({"status": "ok"})


@login_required
def get_accounts(request):
    accounts = InstagramAccounts.objects.select_related('account').filter(
        profile=request.user.userprofile
    )
    primary = request.user.userprofile.primary_account is not None
    response = [{'account': {
        'username': a.account.username, 'biography': a.account.biography,
        'website': a.account.website, 'name': a.account.name,
        'profile_picture': a.account.profile_picture_url,
        'media_count': a.account.media_count,
        'followers_count': a.account.followers_count,
        'follows_count': a.account.follows_count
    }, 'status': a.status, 'set_link': a.id} for a in accounts]
    return JsonResponse(dict(accounts=response, primary=primary))


@login_required
def select_account(request, ig_ac_id):
    instagram_account = get_object_or_404(InstagramAccounts, pk=ig_ac_id)
    instagram_account.status = True
    instagram_account.save()
    graph_media_updater.delay(request.user.id)
    return JsonResponse(
        {'status': 'ok', 'acid': ig_ac_id, 'user': request.user.username}
    )


@login_required
def get_progress(request):
    profile = request.user.userprofile
    account = profile.primary_account
    response = dict(media_count=0, current_count=0, percentage=0)
    if account is not None:
        response = dict(
            media_count=int(account.account.media_count),
            current_count=account.account.posts.filter(partial=False).count()
        )
        if response['media_count'] == 0:
            rate = 1
        elif response['current_count'] == 0:
            rate = 0
        else:
            rate = response['media_count'] / response['current_count']
        response['percentage'] = rate * 100
    return JsonResponse(response)


# TODO: Exchange short_trem access_token to a long_term:
# https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing
