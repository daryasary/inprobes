from django.conf.urls import url, include

from dashboard import views

urlpatterns = [
    url(r'^api/', include('dashboard.api.urls')),
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^profile/$', views.profile_settings, name='profile_settings'),
    url(r'^hashtag/$', views.hashtag_campaign, name='hashtag_campaign'),
    url(r'^hashtag/create/$', views.create_hashtag, name='create_hashtag'),
    url(r'^comment/$', views.comment_campaign, name='comment_campaign'),
    url(r'^comment/create/$', views.create_comment, name='create_comment_step_1'),
    url(r'^comment/create/(?P<pid>[0-9]+)/$', views.create_comment, name='create_comment_step_2'),
]
