from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db.models import Count, Sum, Avg
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404

from django.utils.translation import ugettext_lazy as _

from campaign.models import HashtagCampaign, CommentCampaign
from services.graph_api.models import Tag, Media
from utils.decorators import validate_account
from utils.graph_api import InstagramGraphHandler


@login_required
@validate_account
def dashboard(request):
    user = request.user
    profile = user.userprofile
    account = profile.primary_account.account

    types = dict()
    types_query = account.posts.values('media_type').annotate(count=Count('id'))
    for tq in types_query:
        types[tq['media_type'].lower()] = tq['count']

    like = dict(
        sum=account.posts.aggregate(Sum('like_count')).values()[0],
        avg=account.posts.aggregate(Avg('like_count')).values()[0],
    )

    comment = dict(
        sum=account.posts.aggregate(Sum('comments_count')).values()[0],
        avg=account.posts.aggregate(Avg('comments_count')).values()[0],
    )

    if account.posts.count() > 20:
        chart = account.posts.reverse()[account.posts.count() - 20:]
    else:
        chart = account.posts.reverse()

    table = account.posts.order_by('-like_count', '-comments_count')[:8]

    context = dict(profile=profile, user=user, nav_bar_side='dashboard',
                   types=types, like=like, comment=comment, account=account,
                   chart=chart, table=table)
    return render(request, 'dashboard/index.html', context=context)


@login_required
# @validate_account
def hashtag_campaign(request):
    user = request.user
    profile = user.userprofile
    context = dict(profile=profile, user=user, nav_bar_side='hashtags')
    if not profile.hashtag_campaigns.exists():
        return redirect(reverse('create_hashtag'))
    context['campaigns'] = profile.hashtag_campaigns.all()
    return render(request, 'dashboard/hashtag_campaign/index.html', context)


@login_required
# @validate_account
def create_hashtag(request):
    user = request.user
    profile = user.userprofile

    if not profile.can_add_hashtag():
        messages.add_message(
            request, messages.INFO,
            _('You reached maximum account hashtags, '
              'to add new hashtag upgrade please')
        )
        return redirect(reverse('financial'))
    if request.POST:
        hid = request.POST.get('hashtagId', None)
        if hid is None:
            messages.error(request, _("Hashtag is not selected correctly"))
            context = dict(user=user, profile=profile, nav_bar_side='hashtags')
            return render(request, 'dashboard/hashtag_campaign/create.html',
                          context)
        hashtag = get_object_or_404(Tag, id=hid)
        tag_user = True if request.POST.get('tagUser', 'yes') == 'yes' else False
        if HashtagCampaign.objects.filter(
            user=profile, account=profile.primary_account.account,
            tag=hashtag, active=True).exists():
            messages.error(request, _("Hashtag is not selected correctly"))
            context = dict(user=user, profile=profile, nav_bar_side='hashtags')
            return render(request, 'dashboard/hashtag_campaign/create.html',
                          context)
        campaign = HashtagCampaign.objects.create(
            user=profile, account=profile.primary_account.account,
            tag=hashtag, should_tag_user=tag_user
        )
        return redirect('hashtag_campaign')

    if request.is_ajax():
        slug = request.GET.get('hashtag')
        if len(slug.split()) > 1:
            return HttpResponse(_("Hashtag may not contain space"), status=400)
        handler = InstagramGraphHandler(
            access_token=user.userprofile.facebook_token, hashtag_name=slug,
            account_id=user.userprofile.primary_account.account.id,
        )
        tag_data = handler.hashtag_search.get()

        if isinstance(tag_data, dict):
            tag, create = Tag.objects.get_or_create(id=tag_data['data'][0]['id'], name=slug)
            tag_data['name'] = tag.name
            tag_data['hid'] = tag.id
            return JsonResponse(tag_data)
        if tag_data.status_code // 100 != 2:
            return HttpResponse(
                _("Your instagram token is expired and your "
                  "are not allowed to create campaign"),
                status=tag_data.status_code
            )

    context = dict(user=user, profile=profile, nav_bar_side='hashtags')
    return render(request, 'dashboard/hashtag_campaign/create.html', context)


@login_required
# @validate_account
def comment_campaign(request):
    user = request.user
    profile = user.userprofile
    context = dict(profile=profile, user=user, nav_bar_side='comments')
    if not profile.comment_campaigns.exists():
        return redirect(reverse('create_comment_step_1'))
    context['campaigns'] = profile.comment_campaigns.all()
    return render(request, 'dashboard/comment_campaign/index.html', context)


@login_required
# @validate_account[]
def create_comment(request, pid=None):
    user = request.user
    profile = user.userprofile
    account = profile.primary_account.account
    context = dict(profile=profile, user=user, nav_bar_side='comments', account=account)
    if pid is None:
        context['posts'] = account.posts.order_by('-timestamp')
        return render(request, 'dashboard/comment_campaign/posts_list.html', context)
    media = Media.objects.get(id=pid)
    if request.POST:
        description = request.POST.get('description', None)
        if CommentCampaign.objects.filter(user=profile, media=media).exists():
            messages.error(request, _("This campaign is created previously"))
        else:
            CommentCampaign.objects.create(
                user=profile, account=account, media=media, description=description
            )
            messages.info(request, _("Campaign created successfully"))
        return redirect(reverse('comment_campaign'))
    context['post'] = media
    return render(request, 'dashboard/comment_campaign/create.html', context)


@login_required
def profile_settings(request):
    user = request.user
    profile = user.userprofile
    context = {'nav_bar_side': 'settings', 'profile': profile, 'user': user}
    return render(request, 'dashboard/profile.html',context=context)
