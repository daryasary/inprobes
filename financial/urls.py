from django.conf.urls import url
from .views import *

# TODO: Show user financial status and payment history here
urlpatterns = [
    url(r'^upgrade_account/(?P<pid>[0-9]+)/$', upgrade_account,
        name='upgrade_account'),

]
