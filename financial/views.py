from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, redirect

from financial.models import AccountPackage, AccountPurchase
from payment.models import Payment


@login_required
def upgrade_account(request, pid):
    user = request.user
    package = get_object_or_404(AccountPackage, pk=pid)
    payment_data = {'user': user, 'amount': package.price, 'email': user.email}
    payment = Payment.objects.create(**payment_data)
    purchase_data = {'package': package, 'payment': payment, 'user': user}
    purchase = AccountPurchase.objects.create(**purchase_data)
    return redirect(payment.get_absolute_url())
