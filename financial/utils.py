from suds.client import Client
from .models import Payment, PurchasePackages
from django.conf import settings


def request_zarinpal_payment(user, payment):
    try:
        zarinpal = Gateway.objects.get(type=Gateway.ZARINPAL, active=True)
    except:
        return 'Gateway is not available', False
    else:
        client = Client(zarinpal.webservice)
        result = client.service.PaymentRequest(
            zarinpal.merchant_id,
            payment.price,
            payment.description,
            user.email,
            zarinpal.callback
        )
        if result.Status == 100:
            payment.authority = result.Authority
            payment.save()
            return zarinpal.payment_url.format(result.Authority), True



def verify_payment(request):
    # client = Client(ZARINPAL['WEBSERVICE'])
    # if request.args.get('Status') == 'OK':
    #     payment = Payment.objects.get(authority=request.args['Authority'])
    #     result = client.service.PaymentVerification(ZARINPAL['MERCHANT_ID'],
    #                                                 payment.authority,
    #                                                 payment.amount)
    #     if result.Status == 100:
    #         payment.checked(result.RefID)
    #         return payment, True
    #     elif result.Status == 101:
    #         return payment, True
    #     else:
    #         return 'Transaction failed. Status: ' + str(result.Status), False
    # else:
    #     return 'Transaction failed or canceled by user', False
    pass
