from __future__ import unicode_literals

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class FinancialConfig(AppConfig):
    name = 'financial'
    verbose_name = _('Financial')
    verbose_name_plural = _('Financial')

    def ready(self):
        import financial.signals
