from django.contrib import admin

from financial.models import AccountPackage, AccountPurchase


class AccountPackageAdmin(admin.ModelAdmin):
    list_display = ['title',  'interval', 'price', 'enable', 'promotion', 'order_index', 'level']
    list_editable = ['enable', 'promotion', 'order_index']


class AccountPurchaseAdmin(admin.ModelAdmin):
    list_display = ['user',  'package', 'payment', 'valid', 'create_time', 'expire_time']


admin.site.register(AccountPackage, AccountPackageAdmin)
admin.site.register(AccountPurchase, AccountPurchaseAdmin)
