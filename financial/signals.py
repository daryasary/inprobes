from financial.models import Payment
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone


@receiver(post_save, sender=Payment)
def create_user_profile(sender, instance, created, **kwargs):
    if instance.is_paid and not instance._is_paid:
        purchase = instance.purchase
        purchase.valid = True
        purchase.expire_time = timezone.now() + purchase.package.delta
        purchase.save()

        instance.user.userprofile.update_level(instance.purchase.package.level)
