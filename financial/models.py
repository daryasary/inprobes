from __future__ import unicode_literals

from datetime import timedelta

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from index.models import UserProfile

from payment.models import Payment

widget_types = settings.WIDGETS


# class PurchaseManager(models.Manager):
#     def get_queryset(self):
#         return super(PurchaseManager, self).get_queryset().filter(valid=True)
#
#
# class PurchasePackages(models.Model):
#     SERVICE_TYPES = (
#         (widget_types['Festival'], _("Festival")),
#         (widget_types['Campaign'], _("Campaign")),
#         (widget_types['Account'], _("Account")),
#     )
#     service = models.IntegerField(_("Service"), choices=SERVICE_TYPES,
#                                   default=1)
#     payment = models.OneToOneField(Payment, related_name='purchase_package')
#     user = models.ForeignKey(User, related_name='purchase_package')
#     valid = models.BooleanField(default=False)
#     create_time = models.DateTimeField(auto_now_add=True)
#
#     # Generic relations
#     content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
#     object_id = models.PositiveIntegerField()
#     content_object = GenericForeignKey()
#
#     # Model managers
#     objects = models.Manager()
#     is_valid = PurchaseManager()
#
#     class Meta:
#         verbose_name_plural = 'PurchasePackages'
#
#     def __unicode__(self):
#         return '{}___{}'.format(self.user, self.get_service_display())
#
#     @property
#     def measure(self):
#         # TODO: According to content type should return proper remain
#         return 100
#
#     @property
#     def paid(self):
#         return self.payment.is_paid
#
#     def set_used(self):
#         self.valid = False
#         self.save()
#
#     def validate(self):
#         self.valid = True
#         self.save()
#
#
# class BasePackageModel(models.Model):
#     SERVICE_TYPES = (
#         (widget_types['Festival'], _("Festival")),
#         (widget_types['Campaign'], _("Campaign")),
#         (widget_types['Account'], _("Account")),
#     )
#     service = models.IntegerField(_("Service"),
#                                   choices=SERVICE_TYPES,
#                                   default=1)
#     original_price = models.IntegerField(_('Original price'),
#                                          null=True,
#                                          blank=True)
#     price = models.IntegerField(_('Price'))
#     title = models.CharField(_('Title'), max_length=48, null=True, blank=True)
#     description = models.TextField(_('Description'), null=True, blank=True)
#
#     created_at = models.DateTimeField(auto_now=True)
#     modified_at = models.DateTimeField(auto_now_add=True)
#
#     # purchase = GenericForeignKey(PurchasePackages) # Contains error
#
#     class Meta:
#         abstract = True
#
#
# class CampaignPackages(BasePackageModel):
#     interval = models.IntegerField(_('every'), null=False,
#                                    help_text=_('valid periods in day format'))
#
#     def __unicode__(self):
#         return '{} days {}$'.format(self.interval, self.price)
#
#     @property
#     def delta(self):
#         return timedelta(**{'days': self.interval})
#
#
# class FestivalPackages(BasePackageModel):
#     WEEKLY = 1
#     MONTHLY = 2
#     ANNUAL = 3
#     INTERVAL_TYPE = (
#         (WEEKLY, _('weekly')),
#         (MONTHLY, _('monthly')),
#         (ANNUAL, _('annual'))
#     )
#     participants = models.IntegerField(_('Participants'))
#     interval = models.IntegerField(
#         _('Interval'),
#         default=WEEKLY,
#         choices=INTERVAL_TYPE
#     )
#
#     class Meta:
#         verbose_name_plural = 'FestivalPackages'
#
#     def __unicode__(self):
#         return '{} participants {}$'.format(self.participants, self.price)
#
#     @classmethod
#     def load_limited(cls, period):
#         if period in range(8):
#             queryset = cls.objects.filter(interval=cls.WEEKLY)
#         elif period in range(7, 31):
#             queryset = cls.objects.filter(interval=cls.MONTHLY)
#         elif period > 31:
#             queryset = cls.objects.filter(interval=cls.ANNUAL)
#         else:
#             queryset = cls.objects.all()
#         return queryset
#
#
# class AdditionalFestivalPackages(BasePackageModel):
#     referrer = models.ForeignKey(FestivalPackages, related_name=_('additional'))
#     additional_participants = models.IntegerField(_('Additional participants'))
#
#     def __unicode__(self):
#         return '{} participants {}$'.format(self.participants, self.price)


# class AccountPackages(BasePackageModel):
#     level = models.CharField(verbose_name=_('level'),
#                              choices=UserProfile.LEVELS,
#                              max_length=1, default='L')
#     interval = models.IntegerField(_('every'),
#                                    null=False,
#                                    help_text=_('valid periods in day format'))
#
#     def __unicode__(self):
#         return 'Upgrade to {} for {} days with {}$'.format(
#             self.get_level_display(),
#             self.interval,
#             self.price)
#
#     @property
#     def delta(self):
#         return timedelta(**{'days': self.interval})

class AccountPackage(models.Model):
    level = models.CharField(
        verbose_name=_('level'),
        choices=UserProfile.LEVELS,
        max_length=1, default=UserProfile.MEDIUM
    )
    interval = models.IntegerField(
        _('every'), help_text=_('valid periods in day format')
    )
    original_price = models.IntegerField(
        _('Original price'), null=True, blank=True
    )
    price = models.IntegerField(_('Price'))
    title = models.CharField(_('Title'), max_length=48, null=True, blank=True)
    description = models.TextField(_('Description'), null=True, blank=True)
    payment_description = models.TextField(
        _('Payment description'), null=True, blank=True
    )

    order_index = models.PositiveIntegerField(_("Order index"), default=0)
    enable = models.BooleanField(_('Enable'), default=True)
    promotion = models.BooleanField(_('Promotion'), default=True)

    created_at = models.DateTimeField(auto_now=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = _('AccountPackage')
        verbose_name_plural = _('AccountPackages')
        ordering = ['order_index']

    def __unicode__(self):
        return 'Upgrade to {} for {} days with {}$'.format(
            self.get_level_display(),
            self.interval,
            self.price
        )

    @property
    def delta(self):
        return timedelta(**{'days': self.interval})


class AccountPurchase(models.Model):
    package = models.ForeignKey(AccountPackage, related_name='purchase')
    payment = models.OneToOneField(Payment, related_name='purchase')
    user = models.ForeignKey(User, related_name='purchase')
    valid = models.BooleanField(default=False)
    create_time = models.DateTimeField(auto_now_add=True)
    expire_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = _('AccountPurchase')
        verbose_name_plural = _('AccountPurchases')

    def __unicode__(self):
        return 'user: {}, package: {}, payment: {}, valid: {}'.format(
            self.user, self.package, self.payment_id, self.valid
        )

    @property
    def is_valid(self):
        if timezone.now() > self.expire_time:
            self.valid = False
            self.save()
            self.user.userprofile.update_level(UserProfile.LOW)
        return self.valid

    @property
    def remain(self):
        return (self.expire_time - timezone.now()).days
