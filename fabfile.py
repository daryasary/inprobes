from __future__ import with_statement
from fabric.api import *
from contextlib import contextmanager as _contextmanager

tracking = 'hosein@95.211.228.204'
env.hosts = [tracking]
env.activate = 'source /home/inprobes/main/inpenv/bin/activate'
env.directory = '/home/inprobes/main'


@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


def pull(*args):
    with cd('/home/inprobes/main'):
        run('git pull --rebase origin master')


def install_requirement():
    with virtualenv():
        run('pip install -r requirements.txt')


def full_restart():
    run('sudo service supervisor restart')


def restart_webserver():
    run('sudo supervisorctl restart inprobes')


def apply_migrations():
    with virtualenv():
        run("python manage.py migrate")


def full_deploy():
    pull()
    install_requirement()
    apply_migrations()
    # full_restart()
    restart_webserver()


def quick_deploy():
    pull()
    restart_webserver()


def say_hello():
    run('pwd')
